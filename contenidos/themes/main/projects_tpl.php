<?php 
	/* Template Name: Projects */ 

	get_header();
	$section 	= get_the_terms($post->id, 'project-category');
	$section 	= $section[0] ? $section[0]->slug : false;
	$id 		= icl_object_id(11);
?>
    <div class="wrap">
		<h1 class="animation noTopAni"><?php echo get_the_title($id); ?></h1>

		<!-- project list content -->
		<div class="project_list_content">
			<?php 
			if($section){
				// si estamos en una categoría de proyecto
				echo do_shortcode('[project_categories current="'.$section.'"]');
				echo do_shortcode('[project_list type="tag" tag="'.$section.'"]');
			} else {
				if(get_query_var('archive_cat') != ''){
					//si estamos en una categoría de archivo
					$current 	= ' current="'.get_query_var('archive_cat').'"';
					$tag 		= ' tag="'.get_query_var('archive_cat').'"';
					$draw		= '[project_list type="archive"'.$tag.']';
				} else {
					//listamos todos los proyectos o todos los archivos
					if($post->post_name == 'archivo' || $post->post_name == 'archive'){
						// listamos todos los archivos
						$draw		= '[project_list type="archive"]';
					} else {
						// listamos todos los proyectos
						$draw		= '[project_list]';
					}
					$current 	= '';
				}
				echo do_shortcode('[project_categories'.$current.']');
				echo do_shortcode($draw);
			}
			?> 
		</div>
	</div> 
<?php 
	get_footer();
?>

