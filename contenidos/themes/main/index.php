<?php 
	get_header();
	if( is_404() ){
		/* Si la página no existe cambiamos el loop para mostrar el contenido de la home */
		$front_page_id = get_option( 'page_on_front' );
		query_posts( 'page_id='.$front_page_id );
	} 
?>
    <div class="wrap">
	<?php
	while ( have_posts() ) : the_post();
		echo '<h1 class="animation">'.get_the_title().'</h1>';
		the_content();
	endwhile;
?> 
	</div> 
<?php get_footer(); ?>