<?php 
	/* Template Name: Home */ 

	get_header(); 
	while ( have_posts() ) : the_post();
	
?>

	<!-- slider top -->
	<?php echo do_shortcode('[home_slider]'); ?>

	<!-- main text list -->
	<div class="home_module home_text_list animation noTopAni">
		<?php the_content(); ?>		
	</div>

	<div class="home_module home_comments">
		<!-- Comments list -->
		<?php echo do_shortcode('[customer_comment_list type=all]'); ?>
	</div>

	<!-- Link to contact form -->
	<?php 
		$contact_img	= get_option('HomePageContactBackground');
		if ( $contact_img == '' ){
			//imagen por defecto cuando no hemos asignado una nueva
			$contact_img = BASE_URL.'assets/images/home_contact_background.jpg';
		}
	?>
	<div class="home_module contact_link animation">
		<p>
			<a href="#contact_form" class="open_popup" style="background-image:url('<?php echo $contact_img; ?>');">
				<?php _e('LET’S TALK', 'ONESTIC-Team' ); ?>
				<strong><?php _e('Do you have any project you want to tell us about?', 'ONESTIC-Home' ); ?></strong>
			</a>
		</p>
	</div>

	<!-- Link to careers -->
	<?php 
		$job_img	= get_option('HomePageJobsBackground');
		if ( $job_img == '' ){
			//imagen por defecto cuando no hemos asignado una nueva
			$job_img = BASE_URL.'assets/images/jobs_home.jpg';
		}
	?>
	<div class="home_module careers_link animation" style="background-image:url('<?php echo $job_img; ?>');">
		<div class="content">
			<?php 
				$postIDbyLang 	= array('es'=>224,'en'=>231);
				$current_lang 	=  ICL_LANGUAGE_CODE;
				$title 			=  get_post_custom_values('HomeJobsBlock_Title', $postIDbyLang[$current_lang]);
				$text 			=  get_post_custom_values('HomeJobsBlock_Text', $postIDbyLang[$current_lang]);
				$title 			=  isset($title[0]) ? $title[0] : '"HomeJobsBlock_Title" vacio';
				$text 			=  isset($text[0]) ? $text[0] : 'Rellena el custom field "HomeJobsBlock_Text" en la página de empleo';
			?>
			<h5><?php echo $title; ?></h5>
			<p><?php echo $text; ?></p>
		</div>
		<a href="<?php link_to(231); ?>" class="link ChangePage"><?php _e('View jobs', 'ONESTIC-careers' ); ?></a>
	</div>
<?php 
	endwhile;
	get_footer(); 
?>