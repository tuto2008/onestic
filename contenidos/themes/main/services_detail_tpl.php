<?php 
	/* Template Name: Services Page */ 

	get_header(); 
	?>
	    <div class="wrap">
	<?php
	while ( have_posts() ) : the_post();

?>		
		<h1 class="animation"><?php the_title(); ?></h1>

		<!-- Services list -->
		<div class="system_accordion">
			<?php the_content(); ?>	
		</div>

		<!-- Comments list -->
		<div class="services_comment">
		<?php echo do_shortcode('[customer_comment_list]'); ?>
		</div>

		<!-- Link to projects page -->
		<p class="end_link animation noDelay"><a href="<?php link_to(11);?>" class="ChangePage"><?php _e('View projects', 'ONESTIC-Services' ); ?></a></p>
<?php 
	endwhile;
?> 
	</div> 
<?php get_footer(); ?>

