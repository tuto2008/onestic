 <?php $current_lang =  ICL_LANGUAGE_CODE; ?>
    	</div>
        <footer>
        	<div class="wrap">
                <?php 
                    wp_nav_menu( 
                        array( 
                            'theme_location'    => 'social_menu',
                            'container'         => '',
                            'menu_class'        => 'redes',
                            'menu_id'           => 'footer_redes',
                        ) 
                    ); 
                ?>
        		<ul class="footer_item">
                    <?php
                    $show_widget = 'address_'.$current_lang;
                    if(is_active_sidebar($show_widget)){
                        dynamic_sidebar($show_widget);
                    }
                    ?>
        			<li class="emails">
        				<h6>Inquiries</h6>
        				<p>General <span>info@onestic.com</span></p>
    				    <p>Other <span>info@onestic.com</span></p>
        			</li>
        		</ul>
        		<p class="legal">Copyright Onestic 2016©</p>
        	</div>
        </footer>
    </div>
    <div id="popup">
    	<a href="#" class="close">Close</a>
    	
    	<!-- Main Menú POPUP -->
    	<?php 
            wp_nav_menu( 
                array( 
            		'theme_location' 	=> 'Main-menu',
            		'container_id' 		=> 'main_menu',
            		'container_class'	=> 'nav popup_section',
            		'menu_class'		=> 'nav_list',
            		'menu_id'			=> 'main_menu_list'
        	   ) 
            ); 
            $form_mgs                   = "OK:".__('Form successfully sent', 'ONESTIC-forms' )."||KO:".__('Sending form error', 'ONESTIC-forms' );
        ?>
    	
    	<!-- Contact Form POPUP -->
    	<div id="contact_form" class="content_popup popup_section">
            <form name="form_contact" id="form_contact" method="" action="post" class="validate_form">
                <h2><?php _e('Contact Form', 'ONESTIC-forms' ); ?></h2>
    	    	<fieldset class="form">
    		    	<label for="name" class="name required check_text">
    		    		<strong><?php _e('Name', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
    		    		    <input data_error="<?php _e('Your name is required', 'ONESTIC-forms' ); ?>" name="name" id="name" type="text" class="field" placeholder="<?php _e('Name', 'ONESTIC-forms' ); ?>" value="">
                        </div>
    		    	</label>
    		    	<label for="company" class="company required check_text">
    		    		<strong><?php _e('Company', 'ONESTIC-forms' ); ?>:</strong>
    		    		<div class="field_wrap">
                            <input data_error="<?php _e('The company name is required', 'ONESTIC-forms' ); ?>" name="company" id="company" type="text" class="field" placeholder="<?php _e('Company', 'ONESTIC-forms' ); ?>" value="">
                        </div>
    		    	</label>
    		    	<label for="email" class="email required check_email">
    		    		<strong><?php _e('E-mail', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
    		    		    <input data_error="<?php _e('Your email is required', 'ONESTIC-forms' ); ?>" name="email" id="email" type="email" class="field" placeholder="<?php _e('E-mail', 'ONESTIC-forms' ); ?>" value="">
                        </div>
    		    	</label>
    		    	<label for="phone" class="phone">
    		    		<strong><?php _e('Phone', 'ONESTIC-forms' ); ?>:</strong>
    		    		<input name="phone" id="phone" type="text" class="field phone" placeholder="<?php _e('Phone', 'ONESTIC-forms' ); ?>" value="">
    		    	</label>
    		    	<label for="comment" class="comment required check_text">
    		    		<strong><?php _e('Comment', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
    		    		    <textarea data_error="<?php _e('Your comment is required', 'ONESTIC-forms' ); ?>" name="comment" id="comment" class="field" placeholder="<?php _e('Comment', 'ONESTIC-forms' ); ?>"></textarea>
                        </div>
    		    	</label>
    		    	<label for="legal" class="legal">
    		    		<input data_error="<?php _e('You must accept the privacy policy', 'ONESTIC-forms' ); ?>" name="legal" id="legal" type="checkbox" value="Accept">
    		    		<strong><span></span><?php _e('I accept the', 'ONESTIC-forms' ); ?> <a href="#"><?php _e('privacy policy', 'ONESTIC-forms' ); ?></a></strong>
    		    	</label>
                    <span class="EndMsgText"></span>
    		    	<input type="submit" value="<?php _e('Send', 'ONESTIC-forms' ); ?>" name="Send_contact" id="Send_contact" class="btn_send">
                    <div class="footer">
                        <p><?php _e('Or use', 'ONESTIC-forms' ); ?> info@onestic.com</p>
                        <?php 
                            wp_nav_menu( 
                                array( 
                                    'theme_location'    => 'social_menu',
                                    'container'         => '',
                                    'menu_class'        => 'redes',
                                    'menu_id'           => 'contact_redes',
                                ) 
                            ); 
                        ?>
                    </div>
    	    	</fieldset>
                <input type="hidden" name="action" id="action" value="contact_send" />
                <input type="hidden" name="current" id="current" value="form_contact" />
                <input type="hidden" name="form_msg" id="form_msg" value='<?php echo $form_mgs; ?>' />
            </form>
    	</div>

        <?php 
            //Sólo pintamos el formulariosi estamos en la sección de empleo
            if( get_the_ID() == 231 || get_the_ID() == 224) { 
        ?>
        <!-- CV Form POPUP -->
        <div id="apply_form" class="content_popup popup_section">
            <form name="form_applyCV" id="form_applyCV" method="" action="post"  enctype="multipart/form-data" class="validate_form">
                <h2><?php _e('Send CV', 'ONESTIC-forms' ); ?></h2>
                <fieldset class="form">
                    <label for="name_CV" class="name required">
                        <strong><?php _e('Name', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
                            <input data_error="<?php _e('Your name is required', 'ONESTIC-forms' ); ?>" name="name_CV" id="name_CV" type="text" class="field" placeholder="<?php _e('Name', 'ONESTIC-forms' ); ?>:" value="">
                        </div>
                    </label>
                    <label for="age_CV" class="age required">
                        <strong><?php _e('Age', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
                            <input data_error="<?php _e('Your age is required', 'ONESTIC-forms' ); ?>" name="age_CV" id="age_CV" type="text" maxlength="2" class="field number" placeholder="<?php _e('Age', 'ONESTIC-forms' ); ?>:" value="">
                        </div>
                    </label>
                    <label for="email_CV" class="email required">
                        <strong><?php _e('E-mail', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
                            <input data_error="<?php _e('Your email is required', 'ONESTIC-forms' ); ?>" name="email_CV" id="email_CV" type="email" class="field" placeholder="<?php _e('E-mail', 'ONESTIC-forms' ); ?>:" value="">
                        </div>
                    </label>
                    <label for="phone_CV" class="phone">
                        <strong><?php _e('Phone', 'ONESTIC-forms' ); ?>:</strong>
                        <input name="phone_CV" id="phone_CV" type="text" class="field phone" placeholder="<?php _e('Phone', 'ONESTIC-forms' ); ?>:" value="">
                    </label>
                    <label for="why_work_CV" class="why_work required show_label">
                        <strong><?php _e('Why do you want to work at Onestic?', 'ONESTIC-forms' ); ?></strong>
                        <div class="field_wrap">
                            <textarea data_error="<?php _e('Please complete this field', 'ONESTIC-forms' ); ?>" name="why_work_CV" id="why_work_CV" class="field"></textarea>
                        </div>
                    </label>
                    <label for="any_project_CV" class="any_project required show_label">
                        <strong><?php _e('If you could work on any project in the world, which would it be and why?', 'ONESTIC-forms' ); ?></strong>
                        <div class="field_wrap">
                            <textarea data_error="<?php _e('Please complete this field', 'ONESTIC-forms' ); ?>" name="any_project_CV" id="any_project_CV" class="field"></textarea>
                        </div>
                    </label>
                    <label for="favorite_CV" class="favorite required show_label">
                        <strong><?php _e("What's your favourite website/app at the moment?", 'ONESTIC-forms' ); ?></strong>
                        <div class="field_wrap">
                            <textarea data_error="<?php _e('Please complete this field', 'ONESTIC-forms' ); ?>" name="favorite_CV" id="favorite_CV" class="field"></textarea>
                        </div>
                    </label>
                    <label for="comment_CV" class="comment required show_label">
                        <strong><?php _e('Comment', 'ONESTIC-forms' ); ?>:</strong>
                        <div class="field_wrap">
                            <textarea data_error="<?php _e('Your comment is required', 'ONESTIC-forms' ); ?>" name="comment_CV" id="comment_CV" class="field"></textarea>
                        </div>
                    </label>

                    <section class="lang_<?php echo $current_lang; ?>">
                        <label for="CV_file_CV" class="CV_file file">
                            <strong><?php _e('Your CV', 'ONESTIC-forms' ); ?> (PDF / DOC)</strong>
                            <input type="file" name="files[]" class="upload_field" id="CV_file" placeholder="Your CV (PDF / DOC)" accept=".pdf,.docx,.doc"/>
                            <input name="CV_file_data" id="CV_file_data" class="upload_data" type="hidden" value="">
                        </label>
                        <label for="other_file_CV" class="other_file file">
                            <strong><?php _e('Other', 'ONESTIC-forms' ); ?> (PDF / DOC)</strong>
                            <input type="file" name="files[]" class="upload_field" id="other_file_CV" placeholder="Your CV (PDF / DOC)" accept=".pdf,.docx,.doc"/>
                            <input name="other_file_data" id="other_file_data" class="upload_data" type="hidden" value="">
                        </label>
                    </section>

                    <label for="legal_CV" class="legal">
                        <input data_error="<?php _e('You must accept the privacy policy', 'ONESTIC-forms' ); ?>" name="legal_CV" id="legal_CV" type="checkbox" value="Accept">
                        <strong><span></span><?php _e('I accept the', 'ONESTIC-forms' ); ?> <a href="#"><?php _e('privacy policy', 'ONESTIC-forms' ); ?></a></strong>
                    </label>
                    <span class="EndMsgText"></span>
                    <input type="submit" value="<?php _e('Send', 'ONESTIC-forms' ); ?>" id="Send_CV" name="Send_CV" class="btn_send">
                    <div class="footer">
                        <p><?php _e('Or use', 'ONESTIC-forms' ); ?> info@onestic.com</p>
                        <?php
                            //menú redes sociales 
                            wp_nav_menu( 
                                array( 
                                    'theme_location'    => 'social_menu',
                                    'container'         => '',
                                    'menu_class'        => 'redes',
                                    'menu_id'           => 'CV_redes',
                                ) 
                            ); 
                        ?>
                    </div>
                </fieldset>
                <input type="hidden" name="action" id="action" value="send_CV" />
                <input type="hidden" name="current" id="current" value="form_applyCV" />
                <input type="hidden" name="data_jobid" id="data_jobid" value="" />
                <input type="hidden" name="form_msg" id="form_msg" value='<?php echo $form_mgs; ?>' />
            </form>
        </div>
        <?php } ?>
    </div>
    <script type="text/javascript">
        //textos traducidos para los formularios
        var form_translate              = {
            button: "<?php _e('Browser', 'ONESTIC-forms' ); ?>",
            feedback: "",
            feedback2: "<?php _e('selected file', 'ONESTIC-forms' ); ?>",
            removeConfirmation: "<?php _e('Are you sure you want to remove this file?', 'ONESTIC-forms' ); ?>"
        }
        var feedback_CV_file_data       = "<?php _e('Your CV (PDF / DOC)', 'ONESTIC-forms' ); ?>";
        var feedback_other_file_data    = "<?php _e('Other (PDF / DOC)', 'ONESTIC-forms' ); ?>";
    </script>
</body>
</html>