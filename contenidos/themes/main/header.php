<?php 
//evitamos la cache de stilos y js.
//Variar esta versión cada vez que modifiquemos estos archivos y los pasemos a producción
$v = rand(0,1000000);//18;
?>
<!DOCTYPE html>
<head>
    <title><?php bloginfo( 'name' ); ?> | <?php bloginfo( 'description' ); ?></title>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo BASE_URL."assets/fonts.css?v=".$v ?>">
    <link rel="stylesheet" href="<?php echo BASE_URL."assets/style.css?v=".$v ?>">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <script src="<?php echo BASE_URL."assets/jquery-1.12.2.min.js";?>"></script>
    <script src="<?php echo BASE_URL."assets/jquery.visible.js?v=".$v ?>"></script>

    <?php 
        //agregamos script para upload de archivos para la sección de empleo
        if( get_the_ID() == 231 || get_the_ID() == 224) { 
    ?>
    <script src="<?php echo BASE_URL."assets/jquery.filer.min.js";?>"></script>
    <script src="<?php echo BASE_URL."assets/upload.js?v=".$v ?>"></script>
    <?php } ?>

    <?php if( is_front_page() ) { ?>
    <script src="<?php echo BASE_URL."assets/jquery.cycle.all.js";?>"></script>
    <?php } ?>
    <script language="javascript1.1">base_url = '<?php echo BASE_URL; ?>'</script>
    <script src="<?php echo BASE_URL."assets/motion.js?v=".$v ?>"></script>
    <script src="<?php echo BASE_URL."assets/main.js?v=".$v ?>"></script>
</head>

<body <?php body_id(); ?>>
    <div class="modal">
        <header> 
            <div class="wrap">
            	<a href="<?php echo BASE_URL;?>" id="logo" class="ChangePage">{Onestic}</a>
                <a href="#main_menu" id="menu_button">Menú</a>
                <?php do_action('wpml_add_language_selector'); ?>
            </div>    
        </header>
        <div id="content">