<?php 
	/* Template Name: Team */ 

	get_header(); 
?>
    <div class="wrap">
<?php
	while ( have_posts() ) : the_post();

	/* recogemos custom fields para usar en la plantilla */
	$data['links'] 			= get_post_meta($post->ID, 'team_external_links');
?>
	<div class="intro_team animation">
		<h1><?php the_title(); ?></h1>
		<h2><?php echo get_the_excerpt(); ?></h2>
		<div class="text">
			<?php the_content(); ?>	
		</div>
	</div>

	<!-- Team pictures list -->
	<?php echo do_shortcode('[team_list]'); ?>

	<!-- External links -->
	<?php get_external_links_team($data['links']);	?>

	<!-- Partners -->
	<section class="module partners">
		<h3 class="animation"><?php _e('Our partners', 'ONESTIC-Team' ); ?></h3>
		<?php echo do_shortcode('[partner_list]'); ?>
	</section>
<?php 
	endwhile;
?> 
	</div> 
<?php get_footer(); ?>

