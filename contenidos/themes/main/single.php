<?php 
	get_header(); 
	while ( have_posts() ) : the_post();
	$prev_post 		= get_previous_post();
	$next_post 		= get_next_post();
	$id 			= get_the_ID();
	$category 		= get_the_terms( $id, 'project-category' );
	$category 		= $category ? '<li class="category">'.$category[0]->name.'</li>' : '';
?>
		<div class="post_header animation">
			<?php echo get_the_post_thumbnail( $id, 'post_header' ); ?>
		</div>
		<div class="wrap">

			<!-- post navegation -->
			<nav class="post-links animation">
				<ul>
					<li class="preview animation">
						<?php if (!empty( $prev_post )){ ?>
						<a href="<?php echo $prev_post->guid ?>" class="ChangePage"><?php echo __('Prev', 'ONESTIC-Projects' )?></a>
						<?php } else { ?>
						&nbsp;
						<?php }?>
					</li>
					<li class="list animation"><a href="<?php link_to(11);?>" class="ChangePage">List</a></li>
					<li class="next animation">
						<?php if (!empty( $next_post )){ ?>
						<a href="<?php echo $next_post->guid ?>" class="ChangePage"><?php echo __('Next', 'ONESTIC-Projects' )?></a>
						<?php } else { ?>
						&nbsp;
						<?php }?>
					</li>
				</ul>
			</nav>

			<!-- post header -->
			<section class="header">
				<h1 class="animation"><?php the_title(); ?></h1>
				<div class="col_01">
					<ul class="project_info_list animation">
						<li class="client"><?php echo get_post_meta($id, 'project_client', true); ?></li>
						<?php echo $category; ?>
						<?php echo do_shortcode('[project_tags_list id='.$id.']'); ?>
					</ul>
				</div>
				<div class="intro_text animation">
					<?php the_excerpt(); ?>
				</div>
			</section>

			<!-- post content -->
			<section class="post_content">
				<?php the_content(); ?>
			</section>

			<!-- post numbers -->
			<?php echo do_shortcode("[project_units_count id=".$id."]"); ?>
		</div>
<?php
	endwhile; 
	get_footer(); 
?>
