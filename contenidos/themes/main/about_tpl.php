<?php 
	/* Template Name: About */ 

	get_header(); 
	$current_lang =  ICL_LANGUAGE_CODE;
?>
    <div class="wrap">
<?php
	while ( have_posts() ) : the_post();
?>
	<h1 class="animation"><?php the_title(); ?></h1>

	<!-- about page content -->
	<?php echo do_shortcode('[about_page]'); ?>

	<!-- Link to contact form -->
	<p class="end_link animation"><a href="#contact_form" class="open_popup"><?php _e('LET’S TALK', 'ONESTIC-Team' ); ?></a></p>

	<!-- Offices -->
	<div class="offices_list">
		<ul>
            <?php
            $show_widget = 'address_'.$current_lang;
            if(is_active_sidebar($show_widget)){
                dynamic_sidebar($show_widget);
            }
            ?>
		</ul>	
	</div>
<?php 
	endwhile;
?> 
	</div> 
<?php get_footer(); ?>

