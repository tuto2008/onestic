<?php 
	/* Template Name: Careers */ 

	get_header(); 
	while ( have_posts() ) : the_post();
?>
	<!-- careers header -->
	<section class="header_careers animation">
		<h1><?php the_title(); ?></h1>
		<?php the_excerpt(); ?>
	</section>

	<!-- careers content -->
	<section class="content_careers">
		<div class="wrap">
			<?php the_content(); ?>
		</div>
	</section>

	<!-- Link to aply popup -->
	<p class="end_link apply animation noDelay"><a href="#apply_form" data_jobid="" class="open_popup"><?php _e('Send your CV', 'ONESTIC-careers' ); ?></a></p>
<?php 
	endwhile;
?> 
	</div> 
<?php get_footer(); ?>

