<?php 

	/* custom rewrite */
	add_action( 'init', 'mis_reglas_rewrites_init' );
	function mis_reglas_rewrites_init(){
		add_rewrite_rule('proyectos/archivo/([^/]*)/?','index.php?pagename=proyectos/archivo&archive_cat=$matches[1]','top' );
		add_rewrite_rule('projects/archive/([^/]*)/?','index.php?pagename=projects/archive&archive_cat=$matches[1]','top' );
	}
	
	add_filter( 'query_vars', 'misreglas_query_vars' );
	function misreglas_query_vars( $query_vars ){
	    $query_vars[] = 'archive_cat';
	    return $query_vars;
	}

	add_action('wp_loaded','my_flush_rules' ); 
	function my_flush_rules(){ 
        $rules = get_option( 'rewrite_rules');
        if ( ! isset( $rules['proyectos/archivo/([^/]*)/?'] ) ) { 
            global $wp_rewrite; 
            $wp_rewrite->flush_rules(); 
        } 
	} 
	/* fin rewrite */

	add_filter('nav_menu_css_class', 			'my_css_attributes_filter', 100, 1);
	add_filter('nav_menu_item_id', 				'my_css_attributes_filter', 100, 1);
	add_filter('page_css_class', 				'my_css_attributes_filter', 100, 1);
	add_action('init', 							'register_onestic_menu' );
	add_action('init', 							'my_add_excerpts_to_pages' );
	add_action('admin_enqueue_scripts', 		'onestic_admin_scripts');
	add_action('admin_menu', 					'remove_menus');
	add_filter('wp_mail_content_type', 			'set_content_type');

	//funciones ajax de los formularios de contacto y CV
	add_action('wp_ajax_contact_send', 			'contact_send_callback');
	add_action('wp_ajax_nopriv_contact_send', 	'contact_send_callback');
	add_action('wp_ajax_send_CV', 				'send_CV_callback');
	add_action('wp_ajax_nopriv_send_CV', 		'send_CV_callback');

	add_theme_support('post-thumbnails', array( 'project', 'team', 'customer_comment', 'about_page' ) );
	set_post_thumbnail_size(400,400,true);

	/* images size for custom post type 'team' */
	add_image_size('team', 400, 400, true);

	/* images size for custom post type 'projects' */
	add_image_size('horizontal_thumb', 800, 400, true);
	add_image_size('vertical_thumb', 400, 800, true);
	add_image_size('post_header', 1400, 500, true);

	/* images size for custom post type 'customer_comment' */
	add_image_size('comment_thumb', 450, 450, true);

	/* images size for custom post type 'about_page' */
	add_image_size('about_thumb', 1180, 784, true);

	function register_onestic_menu() {
	  	register_nav_menu('Main-menu',__( 'Main-menu' ));
	  	register_nav_menu('social_menu','Menú para redes sociales');
	}
	
	function my_css_attributes_filter($var) {
		$id = (!is_array($var)) ? str_replace("menu-item-","nav_",$var) : $var;
		$class = is_array($var) ? array('item','ChangePage') : $id;
		if (is_array($var) && in_array("current_page_item", $var)) {
			$class[] = 'active';
		}
		if (is_array($var) && in_array("open_popup", $var)) {
			$class[] = 'open_popup';
		}
		if (is_array($var) && in_array("link", $var)) {
			$class = array( "link", $var[1] );
		}
	  	return $class;
	}

	function my_add_excerpts_to_pages() {
	     add_post_type_support( 'page', 'excerpt' );
	}

	function remove_menus(){
		remove_menu_page( 'index.php' );                  //Dashboard
		remove_menu_page( 'edit-comments.php' );          //Comments
		remove_menu_page( 'edit.php' );                   //Posts
	}

	function onestic_admin_scripts() {
		global $post_type;
	    if ( (isset($_GET['page']) && $_GET['page'] == 'partners') || $post_type == 'customer_comment' || $post_type == 'project' ) {
	        wp_enqueue_media();
	        wp_register_script('admin.js', WP_PLUGIN_URL.'/personalizacion/admin.js', array('jquery'));
	        wp_enqueue_script('admin.js');

	        // si estamos en la página de partners agregamos ui sortable
	        if(isset($_GET['page']) && $_GET['page'] == 'partners'){
	        	wp_enqueue_script('jquery-ui-sortable' );
	        }
	    }
	}

	function set_content_type( $content_type ) {
		return 'text/html';
	}

	function formMSG_fcn($mensajes){
		$mensajes 			= explode("||", $mensajes);
		$form_msg			= array();

		foreach($mensajes as $key=>$value){
			list($id,$msg)	= explode (":",$value);
			$form_msg[$id]	= $msg;
		}
		return $form_msg;
	}

	function contact_send_callback() {
		//global $wpdb;
		$data 				= $_POST;
		$current 			= $data['current'];
		$form_msg 			= formMSG_fcn($data['form_msg']);

		unset($data['legal']);
		unset($data['action']);
		unset($data['current']);
		unset($data['form_msg']);

		$tpl 				= '<h1>Formulario de Contacto</h1><table width="100%" border="1" cellpadding="4" cellspacing="0" bordercolor="#000"><thead><tr><th width="14%" align="left" bgcolor="#000000"><font color="#fff">Campo</font></th><th align="left" bgcolor="#000000"><font color="#fff">Valor</font></th></tr><tbody>[list]</tbody></thead></table>';
		$item 				= '<tr><th width="14%" align="left" bgcolor="#CCCCCC">{campo}</th><td>{valor}</td></tr>';
		$html 				= '';
		$campos 			= array(
			'name'			=> 'Nombre:',
			'company'		=> 'Empresa:',
			'email'			=> 'E-mail:',
			'phone'			=> 'Teléfono:',
			'comment'		=> 'Comentario:',
		);

		foreach($data as $key=>$value){
			$texto = ($key == 'email') ? '<a href="mailto:'.$value.'">'.$value.'</a>' : $value;
			$data = array(
				'{campo}' => $campos[$key],
				'{valor}' => $texto
			);
			$html.= strtr($item,$data);
		}
		$html 		= str_replace("[list]",$html,$tpl);

		//sacamos estos valores de la sección configuración del backend
		$from 		= get_option('ContactFormEmail');
		$subject 	= get_option('ContactFormSubject');

		if ( wp_mail($from, $subject, $html) ){
			echo json_encode( array(
				'status' 	=> 'OK',
				'msg' 		=> $form_msg['OK'],
				'form'		=> $current) 
			);
		} else {
			echo json_encode( array(
				'status' 	=> 'KO',
				'msg' 		=> $form_msg['KO'],
				'form'		=> $current)
			);
		}
		wp_die();
	}

	function send_CV_callback() {
		global $wpdb;
		$current 			= $_POST['current'];
		$table_name 		= $wpdb->prefix.APPLY_DBTABLE;
		$form_msg 			= formMSG_fcn($_POST['form_msg']);

		$data = array(
			'name' 			=> $_POST['name_CV'],
			'age' 			=> $_POST['age_CV'], 
			'email' 		=> $_POST['email_CV'], 
			'phone'  		=> $_POST['phone_CV'],
			'why_work' 		=> $_POST['why_work_CV'], 
			'project'  		=> $_POST['any_project_CV'],
			'favorite'  	=> $_POST['favorite_CV'],
			'comment'  		=> $_POST['comment_CV'],
			'CV_file' 		=> $_POST['CV_file_data'], 
			'Other_file' 	=> $_POST['other_file_data'], 
			'Offer_ID' 		=> $_POST['data_jobid'],
		);

		if( $wpdb->insert( $table_name,$data ) ){
			echo json_encode( array(
				'status' 	=> 'OK',
				'msg' 		=> $form_msg['OK'],
				'form'		=> $current) 
			);
		} else {
			echo json_encode( array(
				'status' 	=> 'KO',
				'msg' 		=> $form_msg['KO'],
				'form'		=> $current)
			);
		}
		wp_die();
	}
?>