jQuery(document).ready( function( $ ) {
	$('.current_logos ul').sortable({
        update: function( event, ui ) {
            change_data();
        }
    });
	$('.current_logos ul').disableSelection();

    $('.data input').bind({
        /*keypress: change_data,*/
        keyup:      update_data,
        change:     update_data
    });

    $('.cantidades table input').bind({
        keyup:      update_project_units,
        change:     update_project_units
    });

	var custom_uploader;
    $('.upload_img').click(function(e) {

        e.preventDefault();

        var current     = $(this).attr('id');
        var setup_popup = {
            upload_image_button:      {
                title:      'Seleccione el/los logo/s de cliente/s que desea agregar',
                button:     {text: 'Seleccionar imagen/es'},
                multiple:   true
            },
            upload_image_light:       {
                title:      'Seleccione el logotipo para fondos claros',
                button:     {text: 'Seleccionar logotipo'},
                multiple:   false
            },
            upload_image_dark:        {
                title:      'Seleccione el logotipo para fondos oscuros',
                button:     {text: 'Seleccionar logotipo'},
                multiple:   false
            },
        };

        //Extend the wp.media object
        custom_uploader = wp.media.frames.file_frame = wp.media(setup_popup[current]);

         //Cuando seleccionamos un archivo, cogemos la url y asignamo según corresponda
        custom_uploader.on('select', function() {
            var data = custom_uploader.state().get('selection').toJSON();
            switch(current) {
                case 'upload_image_button':
                    add_partners_logos_upload(data);
                break;
                case 'upload_image_light':
                    //comment_logo_light
                    $('input#comment_logo_light').val(data[0].url);
                    $('li.light img').attr('src',data[0].url);
                break;
                case 'upload_image_dark':
                    //comment_logo_light
                    $('input#comment_logo_dark').val(data[0].url);
                    $('li.dark img').attr('src',data[0].url);
                break;
            }
        });

        //Open the uploader dialog
        custom_uploader.open();
    });

    $('.current_logos li span.delete').click(function(e) {
        e.preventDefault();
        if(confirm("¿Estas seguro que deseas eliminar este logotipo?")){
            $(this).closest('li').remove();
            change_data();
        }
    });

    $('.current_logos li span.edit').click(function(e) {
        e.preventDefault();
        var obj = $(this).closest('li');
        if(!obj.hasClass('active')){
            $('.current_logos li').removeClass('active');
            $('input#edit_name').val(obj.attr('data_name'));
            $('input#edit_url').val(obj.attr('data_url'));
            $('input#edit_posi').val(obj.index());
            obj.addClass('active');
            $('.data').slideDown(300);
        } else {
            obj.removeClass('active');
            $('.data').slideUp(300); 
        }
    });

    function add_partners_logos_upload($data){
        var tpl         = '<li data_name="" data_url=""><img src="{img}"></li>';
        var html        = '';
        var current     = $data;
        for (var key in current) {
            html+= tpl.replace("{img}", current[key].url);
        }
        $(html).appendTo(".current_logos ul");
        change_data(); 
    }

    function change_data(){
        var tpl         = '{"partners":{[listado]}}';
        var tpl_item    = '"[cont]":{"nombre":"[nombre]","image":"[image]","url":"[url]"}';
        var mas         = '';
        var json        = '';
        var cont        = 1;
        $('.current_logos ul li').each(function(){
            $nombre     = $(this).attr('data_name');            //$(this).find('label.name input').val()
            $nombre     = $nombre.trim();
            $image      = $(this).find('img').attr('src');      //$(this).find('label.image input').val();
            $image      = $image.trim();
            $url        = $(this).attr('data_url');             //$(this).find('label.url input').val();
            $url        = $url.trim();
            $filled     = $nombre+$image+$url;
            if( $filled.length > 0){
                item    = tpl_item.replace("[nombre]",$nombre);
                item    = item.replace("[image]",$image);
                item    = item.replace("[url]",$url);
                item    = item.replace("[cont]",cont);
                json    = json + mas + item;
                mas     = ",";
            } 
            cont++;  
        });
        if( json.length > 0){
            json = tpl.replace("[listado]",json);
        }
        $('#partners_logos').val(json);
    }

    function update_data(){
        var posi = $('#edit_posi').val();
        $('.current_logos ul li:eq("'+posi+'")').attr('data_name',$('#edit_name').val());
        $('.current_logos ul li:eq("'+posi+'")').attr('data_url',$('#edit_url').val());
        change_data();
    }

    function update_project_units(){
        var tpl         = '{"project_units":{[listado]}}';
        var tpl_item    = '"[cont]":{"number":"[number]","unit":"[unit]","text":"[text]"}';
        var mas         = '';
        var json        = '';
        var cont        = 1;
        $('.cantidades tbody tr').each(function(){
            $number     = $(this).find('td.number input').val();
            $number     = $number.trim();
            $unit       = $(this).find('td.unit input').val();
            $unit       = $unit.trim();
            $text       = $(this).find('td.text input').val();
            $text       = $text.trim();
            $filled     = $number+$unit+$text;
            if( $filled.length > 0){
                item    = tpl_item.replace("[number]",$number);
                item    = item.replace("[unit]",$unit);
                item    = item.replace("[text]",$text);
                item    = item.replace("[cont]",cont);
                json    = json + mas + item;
                mas     = ",";
                cont++;
            }
        });
        if( json.length > 0){
            json = tpl.replace("[listado]",json);
        }
        $('#project_numbers').val(json);
    }
});