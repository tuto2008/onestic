<?php 
	/* images size for custom post type 'projects' */
	set_post_thumbnail_size(400,400,true);
	add_image_size('horizontal_thumb', 800, 400, true);
	add_image_size('vertical_thumb', 400, 800, true);
	add_image_size('post_header', 1400, 500, true);

	$project_fields 						= array('project_client','project_numbers','project_url','project_tohome','project_slider_text');
	$project_values 						= array();

	add_action('init', 						'icl_theme_register_custom', 0);
	add_action('add_meta_boxes', 			'add_project_metaboxes'); 
	add_action('save_post',					'project_save_data');

	add_shortcode('project_list', 			'project_list_func' );
	add_shortcode('project_categories', 	'project_categories_func' );
	add_shortcode('cols', 					'cols_func' );
	add_shortcode('project_tags_list', 		'project_tags_list_func' );
	add_shortcode('project_units_count', 	'project_units_count_func' );
	add_shortcode('home_slider', 			'home_slider_func' );

	function add_project_metaboxes() {
		add_meta_box('project_data', 'Datos Adicionales', 'project_data', 'project', 'normal', 'default');
		add_meta_box('project_to_home', 'Slider de portada', 'project_to_home', 'project', 'normal', 'default');
	}

	function project_to_home(){
		global $post;
		$to_home 		= get_post_meta($post->ID, 'project_tohome', true);
		$slider_text 	= get_post_meta($post->ID, 'project_slider_text', true);
		$checked 		= isset($to_home) && $to_home == 'mostrar' ? ' checked' : '';
?>
		<style>
			.cantidades strong{margin-top:20px;float:left;width:100%}
			.cantidades tbody td input{width: 100%}
			.cantidades .number{width:20%}
			.cantidades .unit{width:10%}
			.cantidades .text{width:70%}
			#project_numbers{display:none}
			.cantidades tbody tr:nth-child(2), .cantidades tbody tr:nth-child(1){background:#F3F3F3}
			label.slider_block{display:block;clear:both;border-top:1px solid #ddd;margin-top:10px;padding-top:5px}
			label.slider_block textarea{width:100%;height:120px}
		</style>
		<label>
			<input type="checkbox" id="project_tohome" name="project_tohome" value="mostrar" class="widefat" <?php echo $checked; ?>/>
			<strong>Mostrar en la home</strong>
		</label>
		<label class="slider_block">
			<strong>Texto para el slider:</strong>
			<textarea name="project_slider_text" id="project_slider_text"><?php echo $slider_text; ?></textarea>
		</label>
<?php 
	}
	
	function project_data(){
		global $post, $project_fields, $project_values;
		foreach($project_fields as $item){
			$project_values[$item]	 = get_post_meta($post->ID, $item, true);
		}
?>
		<label>
			<input type="hidden" name="project_secure_box_nonce" id="project_secure_box_nonce" value="<?php echo wp_create_nonce('project_secure_box'); ?>"/>
		</label>
		<label>
			<strong>Cliente:</strong>
			<input type="text" id="project_client" name="project_client" value="<?php echo $project_values['project_client']; ?>" class="widefat" />
		</label>
		<label>
			<strong>Url del proyecto:</strong>
			<input type="text" id="project_url" name="project_url" value="<?php echo $project_values['project_url']; ?>" class="widefat" />
		</label>
		<label class="cantidades">
			<?php $valores = get_current_project_units($project_values['project_numbers']); ?>
			<strong>Cantidades:</strong><span>Sólo se mostrar en la ficha del proyecto cantidades pares (2 ó 4)</span>
			<table class="wp-list-table widefat fixed">
				<thead>
					<tr> 
						<th class="number">Cantidad</th>
						<th class="unit">Unidad</th>
						<th class="text">Texto</th>
					</tr>
				</thead>
				<tbody>
					<?php for($x=0;$x<=3;$x++) { ?>
					<tr>
						<td class="number"><input onkeyup="this.value=this.value.replace(/[^\d]/,'')" type="text" name="cantidad_<?php echo $x; ?>" id="cantidad_<?php echo $x; ?>" value="<?php echo $valores[$x]['number']; ?>"></td>
						<td class="unit"><input type="text" name="unidad_<?php echo $x; ?>" id="unidad_<?php echo $x; ?>" value="<?php echo $valores[$x]['unit']; ?>"></td>
						<td class="text"><input type="text" name="texto_<?php echo $x; ?>" id="texto_<?php echo $x; ?>" value="<?php echo $valores[$x]['text']; ?>"></td>
					</tr>
					<?php } ?>
				</tbody>
			</table>
			<input type="text" id="project_numbers" name="project_numbers" value='<?php echo $project_values['project_numbers']; ?>' class="widefat" />
		</label>
<?php 
	}

	function project_save_data(){
		global $meta_box, $project_fields, $post;		
		//verify nonce
		if (!wp_verify_nonce($_POST['project_secure_box_nonce'],'project_secure_box')) {
			return $post->ID;
		}
		
		foreach($project_fields as $item){
			update_post_meta($post->ID, $item, $_POST[$item]);
		}
	}

	function icl_theme_register_custom() {
		$labels = array(
				'name'               => 'Proyectos',
				'singular_name'      => 'Proyecto',
				'menu_name'          => 'Proyectos',
				'name_admin_bar'     => 'Proyecto',
				'add_new'            => 'Nuevo',
				'add_new_item'       => 'Agregar proyecto',
				'new_item'           => 'Nuevo proyecto',
				'edit_item'          => 'Editar proyecto',
				'view_item'          => 'Ver proyecto',
				'all_items'          => 'Todos los proyectos',
				'search_items'       => 'Buscar proyecto',
				'parent_item_colon'  => 'Proyecto padre:',
				'not_found'          => 'Proyectos no encontrados.',
				'not_found_in_trash' => 'Proyectos no encontrados en la papelera.'
			);

		$args = array(
			'labels'             	=> $labels,
	        'description'        	=> 'Descripción',
			'public'             	=> true,
			'publicly_queryable' 	=> true,
			'show_ui'            	=> true,
			'show_in_menu'       	=> true,
			'query_var'          	=> true,
			'rewrite'            	=> array( 'slug' => 'project' ),
			'capability_type'    	=> 'post',
			'has_archive'        	=> true,
			'hierarchical'       	=> false,
			'menu_position'      	=> 1,
			'menu_icon'   			=> 'dashicons-lightbulb',
			'supports'           	=> array( 'title', 'editor', 'thumbnail', 'excerpt')
		);
	    register_post_type( 'project', $args );


		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => 'Categorías',
			'singular_name'     => 'Categoría',
			'search_items'      => 'Buscar Categorías',
			'all_items'         => 'Todas las Categorías',
			'parent_item'       => 'Categoría padre',
			'parent_item_colon' => 'Categoría padre:',
			'edit_item'         => 'Editar Categoría',
			'update_item'       => 'Actualizar Categoría',
			'add_new_item'      => 'agregar Categoría',
			'new_item_name'     => 'Nuevo nombre',
			'menu_name'         => 'Categoría'
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'project-category' ),
		);

		register_taxonomy( 'project-category', array( 'project' ), $args );

		// Add new taxonomy, NOT hierarchical (like tags)
		$labels = array(
			'name'                       => 'Etiquetas',
			'singular_name'              => 'Etiqueta',
			'search_items'               => 'Buscar etiquetas',
			'all_items'                  => 'Todas',
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => 'Editar etiquetas',
			'update_item'                => 'actualizar',
			'add_new_item'               => 'nueva etiqueta',
			'new_item_name'              => 'Nombre',
			'separate_items_with_commas' => 'Separar etiquetas con comas',
			'add_or_remove_items'        => 'Agregar o quitar etiquetas',
			'choose_from_most_used'      => 'Elija entre las etiquetas más utilizadas',
			'not_found'                  => 'Etiquetas no encontradas',
			'menu_name'                  => 'Etiquetas',
		);

		$args = array(
			'hierarchical'          	=> false,
			'labels'                	=> $labels,
			'show_ui'               	=> true,
			'show_admin_column'     	=> true,
			//'update_count_callback' 	=> '_update_post_term_count',
			'query_var'             	=> true,
			'rewrite'               	=> array( 'slug' => 'project-tag' ),
		);

		register_taxonomy( 'project-tag', 'project', $args );
	}

	function get_current_project_units($origen){
		$number = json_decode($origen,true);
		$valores = array_fill(0, 4, array('number'=>'','unit'=>'','text'=>''));
		if($number){
			$cont = 0;
			foreach($number['project_units'] as $key=>$value){
				$valores[$cont]['number'] 	= $value['number'];
				$valores[$cont]['unit'] 	= $value['unit'];
				$valores[$cont]['text'] 	= $value['text'];
				$cont++;
			}
		}
		return $valores;
	}

	/* SHORTCODES FUNCTIONS */
	function cols_func( $atts, $content = "" ) {
		$args = shortcode_atts( array(
		    'n'	=> '2',
		    's'	=> '1em'
		), $atts );
		$tpl 	= '<div class="cols_content" style="{style}">'.wpautop(trim($content)).'</div>';//
		$style 	= '-webkit-column-count:{col};-moz-column-count:{col};column-count:{col};-webkit-column-gap:{s};-moz-column-gap:{s};column-gap:{s}';
		$html  	= str_replace("{style}",	$style,			$tpl);
		$html  	= str_replace("{s}",		$args['s'],		$html);
		$html  	= str_replace("{col}",		$args['n'],		$html);
		return $html;
	}

	function project_categories_func( $atts ) {
		global $post;
		$SC_args = shortcode_atts( array(
	        'current'	=> ''
	    ), $atts );

		$post_slug=$post->post_name;

		//diferenciamos el menú de categorias si estamos en proyectos ó archivo
		if($post_slug == 'archive' || $post_slug == 'archivo'){
			$isCustomArchive = true;
			$AllLink = get_permalink();
		} else {
			$isCustomArchive = false;
			$AllLink = link_to(11,false);
		}

		$tpl 				= '<a href="#" class="mobile_expand_menu animation">{current}</a><div class="sections_nav"><ul class="sections"><li class="animation noTopAni"><a href="'.$AllLink.'" class="ChangePage">'.__('All', 'ONESTIC-Projects' ).'</a></li>[items]</ul></div>';
		$item 				= '<li class="noTopAni animation{active}">{name}</li>';
		$terms 				= get_terms('project-category', 'hide_empty=0&orderby=description' );
		$html 				= '';
		$cont 				= 1;
		$current_text 		= '';
		foreach ( $terms as $id=>$term ) {
			if( $SC_args['current'] == $term->slug ){
				$active 		= ' active';
				$current_text 	= $term->name;
			} else {
				$active 		= '';
			}

			$link 			= !$isCustomArchive  ? get_term_link( $term->term_id, 'project-category' ) : $AllLink.$term->slug;
			$name 			= $term->count > 0 ? '<a href="'.$link.'" class="ChangePage">'.$term->name.'</a>' : '<a class="empty" title="'.__('Not available', 'ONESTIC-Projects' ).'">'.$term->name.'</a>';
			$data 			= array(
				'{active}'	=>	$active,
				'{name}'	=>	$name,
			);
			$html.=	strtr($item,$data);
			$cont++;
		}
		$data 			= array(
			'[items]'	=>	$html,
			'{current}'	=>	$current_text != '' ? $current_text : __('All', 'ONESTIC-Projects' )
		);
		return strtr($tpl,$data);
	}

	function project_list_func( $atts ) {
		$SC_args = shortcode_atts( array(
	        'type' 		=> 'main',
	        'tag'		=> ''
	    ), $atts );

	    $archive = $SC_args["type"] == 'archive' ? true : false;
		global $post;

		$archive_link			= '<li class="archive_link animation"><h2><a href="'.link_to(211,false).'" class="ChangePage">'.__('Archive', 'ONESTIC-Projects' ).'</a></h2></li>';
		$tpl 					= '<!-- Project list --><div id="project_list" class="[type]">[list]</div>';
		$item 					= '<li class="item item_0{posi}{category_slug} animation"><h2>{title}</h2><a href="{link}" class="ChangePage"><img src="{image}"><div class="data"><h6>{category}</h6><p class="date">{date}</p></div></a></li>';
		$star_year 				= '<section class="year animation"><label>[year]</label><ul>';
		$end_year				= '';
		$html = $html_group 	= '';
		$cont 					= 1;
		$year 					= 0;

		$args	= array(
		  'post_type' 			=> 'project',
		  'post_status' 		=> 'publish',
		  'posts_per_page' 		=> -1,
		  'orderby'				=> 'date'
	    );

	    if($SC_args["tag"] != ''){
	    	$args['project-category'] = $SC_args["tag"];
	    }

		$my_query = new WP_Query($args);

		if( $my_query->have_posts() ) {
			$suma = $my_query->posts;
			foreach ($my_query->posts as $key => $values) {
				if(get_the_date('Y',$values->ID) != $year && $archive){
					$year = get_the_date('Y',$values->ID);
					$html.=$end_year.str_replace("[year]","$year",$star_year);
					$end_year = '</ul></section>';
				}

				$posi 				= $cont%9 == 0 ? 9 : $cont%9 ;
				$categories 		= get_the_terms( $values->ID , 'project-category' );
				$cat_slug 			= $categories[0] ? $categories[0]->slug : '' ;
				$cat_name 			= $categories[0] ? $categories[0]->name : '' ;
				if(!$archive){
					$pic_size 		= $cont%9 == 1 || $cont%9 == 6  ? 'horizontal_thumb' : 'thumbnail' ;
					$pic_size 		= $cont%9 == 3  ? 'vertical_thumb' : $pic_size ;
				} else {
					$pic_size 		= 'thumbnail' ;
				}

				$pic 				= wp_get_attachment_image_src( get_post_thumbnail_id( $values->ID ) , $pic_size);
				
				$data = array(
					'{posi}' 			=> $posi,
					'{category_slug}' 	=> ' '.$cat_slug,
					'{title}' 			=> $values->post_title,
					'{link}' 			=> get_permalink($values->ID),
					'{image}' 			=> $pic[0],
					'{category}' 		=> $cat_name,
					'{date}' 			=> get_the_date('d.m.Y',$values->ID)
				);

				$html.= strtr($item,$data);
				$cont++;
			}
			$list = !$archive ? '<ul>'.$html.$end_year.$archive_link.'</ul>' : $html.$end_year ;
			$data = array(
				'[list]' 			=> $list,
				'[type]' 			=> $SC_args["type"] == 'archive' ? 'archive' : 'main'
			);
			$html = strtr($tpl,$data);
		}
		wp_reset_query();
		return $html;
	}

	function project_tags_list_func( $atts ) {
		//listamos los tags asociados al post actual (se usa en single.php)
		$args 	= shortcode_atts( array(
	        'id' 	=> ''
	    ), $atts );

	    $item 			= '<li class="tag">{text}</li>';
	    $html 			= '';
	    $tags			= get_the_terms( $args['id'], 'project-tag' );
	    if($tags){
		    foreach($tags as $key=>$value){
				$html.=	str_replace("{text}",$value->name,$item);
		    }
	    }
	    return $html;
	}

	function project_units_count_func( $atts ) {
		$args = shortcode_atts( array(
	        'id'	=> ''
	    ), $atts );
	    $data 		= get_post_meta($args['id'],'project_numbers', true);
		$data 		= get_current_project_units($data);
		foreach($data as $key=>$value){
			//eliminamos del array las unidades que están vacías
			if($value["number"] == '' && $value["unit"] == '' && $value["text"] == ''){
				unset($data[$key]);
			}	
		}
		$total 		= ( floor(count($data) / 2) * 2 );
		if($total > 0){
			$mitad 		= ($total / 2); 
			$tpl 		= '<section class="post_numbers animation display_'.$total.'"><ul class="numbers counters_block">[list]</ul></section>';
			$item 		= '<li class="item"><h6><span class="numscroller" data-max="{number}">0</span>{unit}</h6><p>{text}</p></li>';
			$center 	= '<li class="center">'.get_circle_animation('circle_timer').'</li>'; 
			$html 		= '';
			for($x=0;$x<$total;$x++){
				$changes = array(
					'{number}' 	=> $data[$x]['number'],
					'{unit}' 	=> $data[$x]['unit'],
					'{text}' 	=> $data[$x]['text']
				);
				if($x == $mitad){
					$html.= $center;
				}
				$html.= strtr($item,$changes);
			}
			return str_replace("[list]",$html,$tpl);
		} else {
			//si el total de cifras es menor de 2, no pintamos nada en la ficha
			return "";
		}
	}

	function home_slider_func(){

		//buscamos un máximo de tres proyectos ordenados por fecha y que
		//tengan marcados el custon_field project_tohome = mostrar
		$args	= array(
			'post_type' 		=> 'project',
			'post_status' 		=> 'publish',
			'posts_per_page' 	=> 3,
			'orderby'			=> 'date',
			'meta_key'			=> 'project_tohome',
			'meta_value'		=> 'mostrar'
	    );

		$tpl 					= '<div class="home_slider_top home_module animation"><div class="content_slider"><ul class="slider">[list]</ul><ul class="slider_nav"></ul></div></div>';// animation
		$item 					= '<li class="item noAni item_0{cont}"><div class="img_content" style="background-image:url(\'{imagen}\')"></div><div class="info"><h2>{title}</h2><p>{text}</p><a href="{link}" class="ChangePage">{link_text}</a></div></li>';
		$ani_item				= '<li class="item ani counters_block ani_0{cont}"><div class="img_content">{svg}</div><div class="info"><h2>{title}</h2><p>{text}</p><div class="number"><h6><span class="numscroller" data-max="{number}">0</span>{unit}</h6><p>{number_text}</p></div></div></li>';
		$html 					= '';
	    $my_query 				= new WP_Query($args);

	    $ani_text 				= get_about_items(3);

	    if( $my_query->have_posts() ) {
	    	$cont = 1;
	    	foreach ($my_query->posts as $key => $values) {
	    		$id 				= $values->ID;
	    		$pic 				= wp_get_attachment_image_src( get_post_thumbnail_id( $values->ID ) , 'post_header');
	    		$pic 				= $pic[0] ? $pic[0] : '';
		    	$changes 			= array(
		    		'{cont}' 		=> $cont,
		    		'{imagen}' 		=> $pic,
		    		'{title}' 		=> $values->post_title,
		    		'{text}' 		=> get_post_meta($values->ID, 'project_slider_text', true),
		    		'{link}' 		=> get_permalink($id),
		    		'{link_text}' 	=> 'VIEW CASE STUDY',
		    	);
		    	$changes_ani		= array(
		    		'{cont}'		=> $cont,
		    		'{title}'		=> $ani_text[$cont]["title"],
		    		'{text}'		=> $ani_text[$cont]["intro"],
		    		'{number}'		=> $ani_text[$cont]["cant_h"],
		    		'{unit}'		=> $ani_text[$cont]["unit_h"],
		    		'{number_text}'	=> $ani_text[$cont]["text_h"],
		    		'{svg}' 		=> get_circle_animation('circle_timer'.$cont,false)
		    	);
		    	$html.= strtr($ani_item,$changes_ani).strtr($item,$changes);
		    	$cont++;
	    	}
	    	return str_replace("[list]",$html,$tpl);
		}
		wp_reset_query();
		return "";
	}
	/* END SHORTCODES FUNCTIONS */
?>