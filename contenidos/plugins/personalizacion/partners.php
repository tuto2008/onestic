<?php 
	add_action('admin_init', 				'partners_register_settings' );
	add_shortcode('partner_list', 			'partner_list_func');
	add_action('admin_menu', 				'partners_admin_menu' );

	function partners_register_settings() {
		register_setting( 'partners_module-group', 'partners_logos' );
	}
	
	function partner_list_func() {
		$tpl 							= '<ul class="clients">{listado}</ul>';
		$tpl_item 						= '<li class="item item_{cont} animation"><a target="_blank" href="{url}" title="{nombre}"><img src="{image}" alt="{nombre}"></a></li>';
		$dd 							= get_partners_logos();
		$html 							= "";
		foreach($dd as $number=>$values){
			$cambios = array(
				"{cont}"				=> $number,
				"{nombre}"				=> $values["nombre"],
				"{image}"				=> $values["image"],
				"{url}"					=> $values["url"]
			);
			$html.= strtr($tpl_item,$cambios);
		}
		return str_replace("{listado}",$html,$tpl);
	}

	function get_partners_logos(){
		$partners_logos 				= get_option('partners_logos');
		$obj 							= json_decode ($partners_logos);
		$dd 							= array();
		if(isset($obj)){
			$cont = 1;
			foreach($obj->partners as $id=>$data){
				$dd[$cont]["nombre"] 	= $data->nombre;
				$dd[$cont]["image"] 	= $data->image;
				$dd[$cont]["url"] 		= $data->url;
				$cont++;
			}
		}
		return $dd;
	}

	function partners_admin_menu() {
	    $page_hook_suffix = add_submenu_page( 'edit.php?post_type=team', 'Partners', 'Partners', 'manage_options', 'partners', 'partners_settings_page' );
	}

	function partners_settings_page() {
		$dd 			= get_partners_logos();
		$partners_logos = get_option('partners_logos');
	?>
	<div class="wrap">
		<form name="update_people" id="update_people" method="post" action="options.php">
			<h2>Gestión de logotipos</h2>
			<p>Las imágenes para esta sección deben medir 230x184px.</p>
		    <?php settings_fields( 'partners_module-group' );?>
		    <?php do_settings_sections( 'partners_module-group' );?>
			<style>
				.module{float:left;display:inline;width:100%;padding-top:15px;margin-bottom:15px;border-top:1px solid #ddd}
				.current_logos ul{float:left;display:inline;width:100%}
				.current_logos ul li{margin:3px 3px 3px 0;padding:1px;float:left;width:155px;height:152px}
				.current_logos ul li img{border:1px solid #ddd;width:calc(100% - 2px);height: 124px;float:left;display:inline}
				.current_logos ul li span{cursor:pointer;margin-top:5px;color:#999}
				.current_logos ul li.active{background-color:#DCDCDC}
				.current_logos ul li.active img{border-color:#999}
				.data label{float:left;display:inline;width:75%}
				.data label.name{width:24%;margin-right:1%}
				.data label strong{float:left;display:inline;line-height:28px}
				.data label input{float:right;width:calc(100% - 80px)}
				#partners_logos, .data{display:none}
			</style>

		    <div class="module current_logos">
	        	<div class="data">
					<label for="edit_name" class="name">
						<strong>Nombre:</strong> 
						<input type="text" id="edit_name" name="edit_name" value=""/>
					</label>
					<label for="edit_url" class="url">
						<strong>Url:</strong> 
						<input type="text" id="edit_url" name="edit_url" value=""/>
					</label>
					<input type="hidden" id="edit_posi" name="edit_posi" value="" />
	        	</div>
				<ul>
	    			<?php 
	    				for($x=1;$x<=count($dd);$x++){ 
							$nombre = $dd[$x]["nombre"];
							$image = $dd[$x]["image"];
							$url = $dd[$x]["url"];
	    			?>
			        <li data_name="<?php echo $nombre;?>" data_url="<?php echo $url;?>">
			        	<img src="<?php echo $image;?>">
			        	<span class="delete dashicons dashicons-trash"></span>
			        	<span class="edit dashicons dashicons-edit"></span>
			        </li> 
			    	<?php } ?>
				</ul>
		    	<textarea name="partners_logos" id="partners_logos" cols="30" rows="10"><?php echo $partners_logos;?></textarea>
		    </div>
		    <div class="module actions">
		    	<?php submit_button( 'Guardar cambios', 'primary', 'submit-form', false ); ?>
			    <input type="button" name="upload_image_button" id="upload_image_button" class="button-secondary upload_img" value="Subir logotipo">
		    </div>
		
		</form>
	</div>
	<?php } ?>