<?php
	$campos 									= array('comment_name','comment_position','comment_logo_light','comment_logo_dark');
	$valores 									= array();

	add_action('init', 							'register_customer_comment', 0);
	add_action('add_meta_boxes', 				'add_customer_comment_metaboxes' );
	add_action('save_post',						'customer_comment_save_data');
	add_shortcode('customer_comment_list', 		'customer_comment_list_func' );

	function register_customer_comment() {
		$labels = array(
				'name'               			=> 'Comentarios',
				'singular_name'      			=> 'Comentario',
				'menu_name'          			=> 'Comentarios',
				'name_admin_bar'     			=> 'Comentarios',
				'add_new'            			=> 'Nuevo',
				'add_new_item'       			=> 'Agregar comentario',
				'new_item'           			=> 'Nuevo comentario',
				'edit_item'          			=> 'Editar comentario',
				'view_item'          			=> 'Ver comentarios',
				'all_items'          			=> 'Listar todos',
				'search_items'       			=> 'Buscar comentario',
				'parent_item_colon'  			=> 'Comentario padre:',
				'not_found'          			=> 'Comentario no encontrado.',
				'not_found_in_trash' 			=> 'Comentario no encontrado en la papelera.'
			);

		$args = array(
			'labels'             				=> $labels,
	        'description'        				=> 'Descripción',
			'public'             				=> false,
			'publicly_queryable' 				=> true,
			'show_ui'            				=> true,
			'show_in_menu'       				=> true,
			'query_var'          				=> true,
			'rewrite'            				=> array( 'slug' => 'customer_comment' ),
			'capability_type'    				=> 'post',
			'has_archive'        				=> false,
			'hierarchical'       				=> false,
			'menu_position'      				=> null,
			'menu_icon'   						=> 'dashicons-format-status',
			'supports'           				=> array('title', 'excerpt', 'thumbnail', 'page-attributes')
		);
	    register_post_type( 'customer_comment', $args );
	}

	function add_customer_comment_metaboxes(){
		add_meta_box('customer_comment_metaboxes', 'Información adicional', 'customer_comment_metaboxes_fcn', 'customer_comment');
	}

	// customer_comment_metaboxes Metabox
	function customer_comment_metaboxes_fcn() {
		global $post, $campos, $valores;

		//cogemos los custom fields del post actual
		foreach($campos as $item){
			$valores[$item]	 = get_post_meta($post->ID, $item, true);
		}
		
		//campo oculto de verificación
?>
		<style>
			label.logo strong{clear:both;width:100%;float:left;display:inline;padding-bottom:5px;margin-bottom:5px;border-bottom:1px solid #ddd;margin-top:16px}
			label.logo ul,label.logo ul li{float:left;display:inline}
			label.logo li input{display:block!important;width:100%}
			label.logo ul li{margin-right:12px;margin-bottom:0}
			label.logo li img{width:120px;height:auto}
			label.logo p.mensaje{clear:both;line-height:normal;margin:0}
		</style>
		<input type="hidden" name="customer_comment_metaboxes_noncename" id="customer_comment_metaboxes_noncename" value="<?php echo wp_create_nonce('customer_comment_metaboxes'); ?>" />
		<label for="comment_name">
			<strong>Autor:</strong>
			<input type="text" name="comment_name" value="<?php echo $valores["comment_name"]; ?>" class="widefat" />
		</label>
		<label for="comment_position">
			<strong>Cargo:</strong>
			<input type="text" name="comment_position" value="<?php echo $valores["comment_position"]; ?>" class="widefat" />
		</label>
		<label for="upload_image_button" class="logo">
			<?php 
				$no_photo	= BASE_URL.'assets/images/logotipo_sinfoto.gif';
				$img_light 	= strlen( $valores["comment_logo_light"] ) == 0 ? $no_photo : $valores["comment_logo_light"];
				$img_dark 	= strlen( $valores["comment_logo_dark"] ) == 0 ? $no_photo : $valores["comment_logo_dark"];
			?>
			<strong>Logotipos:</strong>
			<ul>
				<li class="light">
					<img src="<?php echo $img_light; ?>">
					<input type="button" name="upload_image_light" id="upload_image_light" class="button-secondary upload_img" value="fondo claro">
				</li>
				<li class="dark">
					<img src="<?php echo $img_dark; ?>">
					<input type="button" name="upload_image_dark" id="upload_image_dark" class="button-secondary upload_img" value="fondo oscuro">
				</li>
			</ul>
			<input type="hidden" id="comment_logo_light" name="comment_logo_light" value="<?php echo $valores["comment_logo_light"]; ?>" class="widefat" />
			<input type="hidden" id="comment_logo_dark" name="comment_logo_dark" value="<?php echo $valores["comment_logo_dark"]; ?>" class="widefat" />
			<p class="mensaje">El tamaño de las imágenes es de 120x50px. Colores de fondos: (claro: #f7f7f7, oscuro: #2a2627)</p>
		</label>
<?php
	}

	function customer_comment_save_data(){
		global $meta_box, $campos, $post;		
		//verify nonce
		if (!wp_verify_nonce($_POST['customer_comment_metaboxes_noncename'],'customer_comment_metaboxes')) {
			return $post->ID;
		}
		
		foreach($campos as $item){
			update_post_meta($post->ID, $item, $_POST[$item]);
		}
	}

	function customer_comment_list_func( $atts ) {
		$CC_args = shortcode_atts( array(
		    'type'	=> 'single'
		), $atts );

		global $post;
		$args	= array(
		  'post_type' 					=> 'customer_comment',
		  'post_status' 				=> 'publish',
		  'posts_per_page' 				=> -1,
		  'orderby'						=> 'menu_order',
		  'order'						=> 'ASC'
	    );

		if( $CC_args['type'] == 'single' ) {
			$args['posts_per_page'] 	= 1;
			$args['orderby'] 			= 'rand';
		}

	    $my_query = new WP_Query($args);
		if( $my_query->have_posts() ) {
			$nav 					= '<div class="comment_nav"><a href="#" id="prev_nav" class="prev">Prev</a><a href="#" id="next_nav" class="next">Next</a></div>';
			$nav 					= $CC_args['type'] != 'single' && count($my_query->posts) > 1 ? $nav : '';
			$cant					= $CC_args['type'] == 'single' ? 1 : 2;
			$logo					= $CC_args['type'] == 'single' ? 'comment_logo_light' : 'comment_logo_dark';
			$img 					= $CC_args['type'] == 'single' ? '' : '<img class="thumb" src="{big_img}">';
			$tpl 					= '<div class="comment_list{animation}"><ul>[list]</ul>'.$nav.'</div>';
			$item 					= '<li>'.$img.'<div class="content_item"><p class="text">{texto}</p><p class="author"><strong>{autor}</strong>, {cargo}</p><img class="client_logo" src="{logo}" alt=""></div></li>';
			$html 					= '';

			foreach ($my_query->posts as $key => $values) {
				$pic 				= wp_get_attachment_image_src( get_post_thumbnail_id( $values->ID ) , 'comment_thumb');
				$data 				= array(
					'{texto}'		=> $values->post_excerpt,
					'{autor}'		=> get_post_meta($values->ID, 'comment_name', true),
					'{cargo}'		=> get_post_meta($values->ID, 'comment_position', true),
					'{logo}'		=> get_post_meta($values->ID, $logo, true),
					'{big_img}'		=> $pic[0]
				);
				$html.= strtr($item,$data);
				$cont++;
			}
			$data 				= array(
				'{animation}'	=> ' animation',
				'[list]'		=> $html
			);

			wp_reset_query();
			return strtr($tpl,$data);
		} else {
			return "";
		}
	}
?>