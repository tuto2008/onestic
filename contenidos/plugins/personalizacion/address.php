<?php
	add_action('widgets_init', 				'address_widgets');
	add_action('widgets_init', 				'footer_widgets_init');

	//creamos un área en el footer para mostrar widgets
	function footer_widgets_init() {
		register_sidebar( array(
			'name'          => 'Address (EN)',
			'id'            => 'address_en',
			'before_widget' => '<li class="address">',
			'after_widget'  => '</li>',
			'before_title'  => '<h6>',
			'after_title'   => '</h6>',
		) );

		register_sidebar( array(
			'name'          => 'Direcciones (ES)',
			'id'            => 'address_es',
			'before_widget' => '<li class="address">',
			'after_widget'  => '</li>',
			'before_title'  => '<h6>',
			'after_title'   => '</h6>',
		) );
	}
	
	function address_widgets() {
	    register_widget('address_widget');
	}

	// La nueva clase debe heredar de WP_Widget
	class address_widget extends WP_Widget {
		function address_widget() {
			$options = array('classname' => 'address_widget', 'description' => 'Widget para agregar direcciones de la empresa');
			$this->WP_Widget('address_widget', 'Direcciones', $options);
		}

		function form($instance) {
			// Valores por defecto
			$defaults 			= array(
				'oficina' 		=> '', 
				'telefono'		=> '', 
				'email' 		=> '',
				'direccion' 	=> '',
				'coordenadas' 	=> '',
			);
			// Se hace un merge, en $instance quedan los valores actualizados
			$instance 			= wp_parse_args((array)$instance, $defaults);
			// Cogemos los valores
			$oficina 			= $instance['oficina'];
			$telefono 			= $instance['telefono'];
			$email 				= $instance['email'];
			$direccion 			= $instance['direccion'];
			$coordenadas 		= $instance['coordenadas'];
			// Mostramos el formulario
?>
			<p>
			Oficina:
			<input class="widefat" type="text" name="<?php echo $this->get_field_name('oficina');?>" value="<?php echo esc_attr($oficina);?>"/>
			</p>

			<p>
			Teléfono:
			<input class="widefat" type="text" name="<?php echo $this->get_field_name('telefono');?>" value="<?php echo esc_attr($telefono);?>"/>
			</p>

			<p>
			Email:
			<input class="widefat" type="text" name="<?php echo $this->get_field_name('email');?>" value="<?php echo esc_attr($email);?>"/>
			</p>

			<p>
			Dirección:
			<input class="widefat" type="text" name="<?php echo $this->get_field_name('direccion');?>" value="<?php echo esc_attr($direccion);?>"/>
			</p>

			<p>
			Coordenadas:
			<input class="widefat" type="text" name="<?php echo $this->get_field_name('coordenadas');?>" value="<?php echo esc_attr($coordenadas);?>"/>
			</p>
<?php
		}

		function update($new_instance, $old_instance) {
			$instance = $old_instance;
			// Con sanitize_text_field elimiamos HTML de los campos
			$instance['oficina'] 		= sanitize_text_field($new_instance['oficina']);
			$instance['telefono'] 		= sanitize_text_field($new_instance['telefono']);
			$instance['email'] 			= sanitize_text_field($new_instance['email']);
			$instance['direccion'] 		= sanitize_text_field($new_instance['direccion']);
			$instance['coordenadas'] 	= sanitize_text_field($new_instance['coordenadas']);
			return $instance;
		}

		function widget($args, $instance) {
			// Extraemos los argumentos del area de widgets
			extract($args);

			$email			= $instance['email'];
			$coordenadas	= $instance['coordenadas'];
			$direccion 		= $instance['direccion'];
			$link_text		= __('View on GoogleMaps', 'ONESTIC-About' );

			echo $before_widget;
			echo $before_title;
			echo apply_filters('widget_title', $instance['oficina']);
			echo $after_title;
			echo '<address><strong>'.$instance['telefono'].'</strong><a class="mailto_link" href="mailto:'.$email.'">'.$email.'</a><span>'.$direccion.'</span></address><p class="google_link"><a href="http://maps.google.com/maps?q='.$direccion.'" target="_blank">'.$link_text.'</a></p>';
			echo $after_widget;

		}
	}
?>