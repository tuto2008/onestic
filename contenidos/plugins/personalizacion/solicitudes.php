<?php
	if( ! class_exists( 'WP_List_Table' ) ) {
	    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
	}
	class CV_Applicants extends WP_List_Table {
		private $filter;
		private $table_name;
		private $apply_folder = APPLY_PATH;
		private $NO_Offer = 'Ninguna';

		function delete_file($name){
			if($name != ''){
				$file = $this->apply_folder.$name;
				if (file_exists($file)){
					unlink($file);
				}
			}
		}

		function get_data(){
			global $wpdb;
			$this->table_name 	= $wpdb->prefix.APPLY_DBTABLE;

			if(isset($_REQUEST['offer-filter']) && $_REQUEST["action"] == 'delete'){
				$items 		= isset($_POST['ID']) ? implode(",",$_POST['ID']) : '';
				if($items != ''){

					//seleccionamos la ruta de los archivos a eliminar
					$sql = 'SELECT CV_file, Other_file FROM '.$this->table_name.' WHERE ID IN ('.$items.')';
					$files = $wpdb->get_results($sql);

					//borramos los archivos si existen
					foreach($files as $key=>$values){
						$this->delete_file($values->CV_file);
						$this->delete_file($values->Other_file);
					}

					//borramos las solicitudes
					$sql = 'DELETE FROM '.$this->table_name.' WHERE ID IN ('.$items.')';
					$datos = $wpdb->get_results($sql);
				}
			}
			$more = '';

			$sql = "
			SELECT CV.*, p.post_title
			FROM ".$wpdb->posts." p, ".$this->table_name." CV, ".$wpdb->prefix."icl_translations tr
			WHERE CV.Offer_ID = p.post_name 
			AND p.post_type = 'jobs'
			AND tr.element_id = p.ID
			AND tr.language_code = 'es'";

			$sql_empty = "SELECT *, '".$this->NO_Offer."' as post_title FROM 0n3stic_applycv CV WHERE CV.Offer_ID = ''";

			$this->filter = (isset($_REQUEST['offer-filter']) && $_REQUEST['offer-filter'] != '') ? $_REQUEST['offer-filter'] : false;
			if($this->filter){
				if($this->filter == $this->NO_Offer){
					$sql 	= $sql_empty;
					$more 	= '';
				} else {
					$more 	= " AND CV.Offer_ID = '".$this->filter."'";
				}
			} else {
				$more 		= " UNION ".$sql_empty;
			}
			$sql.= $more;
			return $wpdb->get_results( $sql, ARRAY_A );
		}

		function usort_reorder( $a, $b ) {
			// If no sort, default to title
			$orderby = ( ! empty( $_GET['orderby'] ) ) ? $_GET['orderby'] : 'Fecha';
			// If no order, default to asc
			$order = ( ! empty($_GET['order'] ) ) ? $_GET['order'] : 'desc';
			// Determine sort order
			$result = strcmp( $a[$orderby], $b[$orderby] );
			// Send final sort direction to usort
			return ( $order === 'asc' ) ? $result : -$result;
		}

		function get_columns(){
			$columns = array(
				'cb'        			=> '<input type="checkbox" />',
				'post_title'			=> 'Oferta',
				'name' 					=> 'Nombre',
				'datos'					=> 'Información del solicitante',
				'Fecha'					=> 'Fecha',
			);
			return $columns;
		}

		function prepare_items() {
			$columns 					= $this->get_columns();
			$hidden 					= array();
			$sortable 					= $this->get_sortable_columns();
			$this->_column_headers 		= array($columns, $hidden, $sortable);
			$this->items 				= $this->get_data();
			usort( $this->items, array( &$this, 'usort_reorder' ) );
		}

		function pinta_textos($title,$text){
			$tpl = '<label><strong>'.$title.'</strong><p>'.$text.'</p></label>';
			return $tpl;
		}

		function display_rows() {
			$records = $this->items;
			list( $columns, $hidden ) = $this->get_column_info();
			if(!empty($records)){
				foreach($records as $rec){
					echo '<tr id="record_'.$rec->ID.'">';
					foreach ( $columns as $column_name => $column_display_name ) {
						$class = "class='$column_name column-$column_name'";
						$style = "";
						$attributes = $class . $style;

						$tpl 			= '<table class="content_jobs"><thead><tr><th class="age">Edad</th><th>Email</th><th class="phone">Teléfono</th><th class="CV">CV</th><th class="more">Other</th></tr></thead><tbody><tr><td class="age">{age}</td><td>{email}</td><td class="phone">{phome}</td><td class="CV">{pdf_cv}</td><td class="more">{pdf_other}</td></tr></tbody></table><p class="mas_info"><a href="#">Ver más</a><div class="extra_info">{extra}</div></p>'; 

						$questions 		= array(
							__('Why do you want to work at Onestic?', 'ONESTIC-forms' ) => $rec["why_work"],
							__('If you could work on any project in the world, which would it be and why?', 'ONESTIC-forms' ) => $rec["project"],
							__("What's your favourite website/app at the moment?", 'ONESTIC-forms' ) => $rec["favorite"],
							__('Comment', 'ONESTIC-forms' ) => $rec["comment"]
						);
						$question_html 	= '';
						foreach($questions as $title=>$text){
							$question_html.= $this->pinta_textos($title,$text);
						}

						$changes 			= array(
							'{age}'			=> $rec['age'],
							'{email}'		=> '<a href="mailto:'.$rec['email'].'">'.$rec['email'].'</a>',
							'{phome}'		=> $rec['phone'],
							'{pdf_cv}'		=> $rec['CV_file'] ? '<a href="'.str_replace(ROOT,BASE_URL,$this->apply_folder).$rec['CV_file'].'" target="_blank"><span class="dashicons dashicons-admin-links"></span></a>' : '',
							'{pdf_other}'	=> $rec['Other_file'] ? '<a href="'.str_replace(ROOT,BASE_URL,$this->apply_folder).$rec['Other_file'].'" target="_blank"><span class="dashicons dashicons-admin-links"></span></a>' : '',
							'{extra}'		=> $question_html,
						);
						$table_top 			= strtr($tpl,$changes);

						switch ( $column_name ) {
							case 'cb':
								echo '<th class="check-column" scope="row"><input id="cb-select-'.$rec['ID'].'" type="checkbox" name="ID[]" value="'.$rec['ID'].'" /></th>'; 
							break;

							case 'datos':
								echo '<td '.$attributes.'>'.$table_top.'</td>';
							break;

							default:
								echo '<td '.$attributes.'>'.$rec[$column_name].'</td>';
							break;
						}
					}
			      	echo'</tr>';
		   		}
		   	}
		}

		function get_bulk_actions() {
			$actions = array(
				'delete'    => 'Delete'
			);
			return $actions;
		}

		function get_sortable_columns() {
			$sortable_columns 	= array(
				'post_title'  	=> array('post_title',false),
				'name'  		=> array('name',false),
				'Fecha'  		=> array('Fecha',false)
			);
			return $sortable_columns;
		}

		function extra_tablenav( $which ) {
		    global $wpdb, $testiURL, $tablename, $tablet;
		    if ( $which == "top" ){
		    	echo '<div class="alignleft actions bulkactions">';
				$sql = "
				SELECT CV.Offer_ID as ID ,P.post_title as offer
				FROM ".$wpdb->posts." p, ".$this->table_name." CV, ".$wpdb->prefix."icl_translations tr
				WHERE CV.Offer_ID = p.post_name 
				AND p.post_type = 'jobs'
				AND tr.element_id = p.ID
				AND tr.language_code = 'es' 
				group by P.post_title";


		    	//$sql 	= "SELECT CV.Offer_ID as ID ,P.post_title as offer FROM ".$this->table_name." CV, 0n3stic_posts P WHERE CV.Offer_ID = P.ID AND P.post_type = 'jobs' group by P.post_title";
		    	$offers = $wpdb->get_results($sql, ARRAY_A);
		    	$offers[] = array('ID'=>$this->NO_Offer,'offer'=>$this->NO_Offer);
		    	//var_dump($offers);
		    	if( $offers ){
		    		echo '<select name="offer-filter" class="offer-filter"><option value="">Filtrar por Oferta</option>';
		    		foreach( $offers as $offer ){
		    			$selected = $this->filter == $offer['ID'] ? 'selected' : '';
						echo '<option value="'.$offer['ID'].'" '.$selected.'>'.$offer['offer'].'</option>';
		    		}
		            echo '</select>';
		            echo '<input type="submit" value="Filtrar" class="button" id="offer-filter-submit" name="filter_action">';
		    	}
		        echo '</div>';
		    }
		    if ( $which == "bottom" ){
		    	//no mostramos nada en el pie de la tabla
		    }
		}
	}
?>