<?php
	/*
	Plugin Name: Personalización web ONESTIC
	Description: Plugin para personalizar Wordpress
	Version: 1.0
	Author: Luis Marcano
	Author URI: http://www.luismarcano.com
	*/

	include('setup.php');
	include('functions.php');
	include('projects.php');
	include('jobs.php');
	include('team.php');
	include('partners.php');
	include('customer_comments.php');
	include('address.php');
	include('about_onestic.php');
	include('config_variables.php');
?>