<?php 
	add_action('init', 						'register_jobs', 0);
	add_shortcode('jobs_list', 				'jobs_list_func' );
	add_action('admin_menu', 				'solicitudes_submenu_page');

	function solicitudes_submenu_page() {
		add_submenu_page(
		'edit.php?post_type=jobs',
		'Solicitudes',
		'Solicitudes',
		'manage_options',
		'applicant',
		'applicant_page' 
		);
	}

	function applicant_page() { 
		include('solicitudes.php');
?>
	    <div class="wrap">
	        <h1>Solicitudes recibidas</h1>
	        <style>
				.column-datos, .column-datos{width:54%}
				.column-post_title{width:13%}
				table.content_jobs{width:100%;border:1px solid #e1e1e1;border-spacing:0}
				table.content_jobs thead tr{background:#e1e1e1}
				table.content_jobs th{ padding:0;}
				table.content_jobs td{border-top: 1px solid #ccc;border-right: 1px solid #ccc;padding: 4px 10px;}
				table.content_jobs .age, table.content_jobs .CV, table.content_jobs .more{width:9%; text-align: center}
				table.content_jobs .phone{width:22%}
				table.content_jobs .more{border-right:0}
				table.jobs_page_applicant .extra_info{float:left;display:none;width:100%}
	        </style>
	        <script>
	        	var base_url = '<?php echo BASE_URL; ?>';
	        	jQuery(document).ready( function( $ ) {
	        		$('p.mas_info a').click(function(){
	        			$(this).closest(".column-datos").find('.extra_info').slideToggle(300);
	        			return false;
	        		});
	        	});
	        </script>
	        <form id="applicants_list" method="post">
				<?php 
				$CV_Applicants = new CV_Applicants();
				$CV_Applicants->prepare_items();
				$CV_Applicants->display(); 
				?>
				<input type="hidden" name="post_type" id="post_type" value="jobs"></input>
				<input type="hidden" name="page" id="page" value="applicant"></input>
				<input type="hidden" name="orderby" id="orderby" value="Fecha"></input>
				<input type="hidden" name="order" id="order" value="desc"></input>
			</form>
	    </div>
<?php   
	}

	function register_jobs() {
		$labels = array(
				'name'               => 'Ofertas',
				'singular_name'      => 'Oferta',
				'menu_name'          => 'Ofertas',
				'name_admin_bar'     => 'Oferta',
				'add_new'            => 'Nuevo',
				'add_new_item'       => 'Agregar oferta',
				'new_item'           => 'Nuevo oferta',
				'edit_item'          => 'Editar oferta',
				'view_item'          => 'Ver oferta',
				'all_items'          => 'Todos las ofertas',
				'search_items'       => 'Buscar Oferta',
				'parent_item_colon'  => 'Oferta padre:',
				'not_found'          => 'Ofertas no encontradas.',
				'not_found_in_trash' => 'Ofertas no encontradas en la papelera.'
			);

		$args = array(
			'labels'             	=> $labels,
	        'description'        	=> 'Descripción',
			'public' 				=> true,
			'exclude_from_search' 	=> true,
			'publicly_queryable'  	=> false,
			'show_ui'            	=> true,
			'show_in_nav_menus'   	=> true,
			'show_in_menu'       	=> true,
			'query_var'           	=> true,
			'rewrite'            	=> array( 'slug' => 'jobs' ),
			'capability_type'    	=> 'post',
			'has_archive'        	=> false,
			'hierarchical'       	=> false,
			'menu_position'      	=> 2,
			'menu_icon'   			=> 'dashicons-businessman',
			'supports'           	=> array( 'title', 'editor', 'excerpt')
		);
	    register_post_type( 'jobs', $args );
	}

	function jobs_list_func( $atts ) {
		global $post;

		$args	= array(
		  'post_type' 			=> 'jobs',
		  'post_status' 		=> 'publish',
		  'posts_per_page' 		=> -1,
		  'orderby'				=> 'date'
	    );

		$my_query = new WP_Query($args);

		$tpl 			= '<ul class="animation noDelay items_group{single}">[list]</ul>'; 
		$info_link 		= '<a href="#" class="more_info">'.__('More information', 'ONESTIC-careers' ).'</a>';
		$close_link 	= '<a href="#" class="close">'.__('Close', 'ONESTIC-careers' ).'</a>';
		$apply_link 	= '<p class="apply open_popup"><a href="#apply_form" data_jobID = "{jobID}">'.__('Apply', 'ONESTIC-careers' ).'</a></p>';
		$item 			= '<li class="animation noDelay item item_{cont}{active}"><h2>{title}</h2><p class="intro">{intro}</p>'.$info_link.$close_link.'<div class="list">{content}'.$apply_link.'</div></li>';
		$html 			= "";
		$cont 			= 1;

		if( $my_query->have_posts() ) {
			if( count($my_query->posts) == 1 ){
				$single = ' single';
				$active = ' active';
			} else {
				$single = '';
				$active = '';
			}
			
			foreach ($my_query->posts as $key => $values) {
				$data = array(
					'{cont}'	=> $cont,
					'{title}'	=> $values->post_title,
					'{intro}'	=> $values->post_excerpt,
					'{content}'	=> wpautop($values->post_content),
					'{jobID}'	=> $values->post_name,
					'{active}'	=> $active,
				);
				$html.= strtr($item,$data);
				$cont++;
			}
			wp_reset_query();
			$data = array(
				'{single}'	=> $single,
				'[list]'	=> $html
			);
			return strtr($tpl,$data);
		} else {
			$pageIDbyLang 	= array('es'=>224,'en'=>231);
			$current_lang 	= ICL_LANGUAGE_CODE;
			$text 			= get_post_custom_values('JobPageNoItems', $pageIDbyLang[$current_lang]);
			$text 			= isset($text[0]) && $text[0] != '' ? '<div class="noitems_block">'.$text[0].'</div>' : '';
			return $text;
		}
	}
?>