<?php
	add_action('init', 						'register_about', 0);
	add_action('add_meta_boxes', 			'add_about_metaboxes' );
	add_action('save_post',					'about_save_data');
	add_shortcode('about_page', 			'about_page_func' );

	$about_fields 							= array('about_cant_h','about_unit_h','about_text_h','about_cant_p','about_unit_p','about_text_p');
	$about_values 							= array();

	function register_about() {
		$labels = array(
				'name'               => 'Contenido',
				'singular_name'      => 'Contenido',
				'menu_name'          => 'About',
				'name_admin_bar'     => 'Contenido',
				'add_new'            => 'Nuevo',
				'add_new_item'       => 'Agregar Contenido',
				'new_item'           => 'Nuevo Contenido',
				'edit_item'          => 'Editar Contenido',
				'view_item'          => 'Ver Contenido',
				'all_items'          => 'Todos los contenidos',
				'search_items'       => 'Buscar Contenido',
				'parent_item_colon'  => 'Contenido padre:',
				'not_found'          => 'Contenido no encontrado.',
				'not_found_in_trash' => 'Contenido no encontrado en la papelera.'
			);

		$args = array(
			'labels'             	=> $labels,
	        'description'        	=> 'Descripción',
			'public'             	=> true,
			'exclude_from_search' 	=> true,
			'publicly_queryable'  	=> false,
			'show_ui'            	=> true,
			'show_in_menu'       	=> true,
			'query_var'          	=> true,
			'show_in_nav_menus'   	=> true,
			'rewrite'            	=> array( 'slug' => 'about_page' ),
			'capability_type'    	=> 'post',
			'has_archive'        	=> false,
			'hierarchical'       	=> false,
			'menu_position'      	=> 3,
			'menu_icon'   			=> 'dashicons-id',
			'supports'           	=> array('title', 'editor', 'excerpt', 'thumbnail', 'page-attributes')
		);
	    register_post_type( 'about_page', $args );
	}

	function get_about_items($total = -1){
		global $post;
		$args	= array(
		  'post_type' 						=> 'about_page',
		  'post_status' 					=> 'publish',
		  'posts_per_page' 					=> $total,
		  'orderby'							=> 'menu_order',
		  'order'							=> 'ASC'
	    );
	    $cont 								= 1;
	    $data 								= array();
	    $my_query = new WP_Query($args);
		if( $my_query->have_posts() ) {
			foreach ($my_query->posts as $key => $values) {
				$pic 						= wp_get_attachment_image_src( get_post_thumbnail_id( $values->ID ) , 'about_thumb');
				$data[$cont]['title'] 		= 	$values->post_title;
				$data[$cont]['contenido'] 	= 	str_replace("<br />","</p><p>",wpautop($values->post_content,true));
				$data[$cont]['intro'] 		= 	$values->post_excerpt;
				$data[$cont]['img'] 		= 	isset($pic[0]) ? $pic[0] : '';
				$data[$cont]['cant_h'] 		= 	get_post_meta($values->ID, 'about_cant_h', true);
				$data[$cont]['unit_h'] 		= 	get_post_meta($values->ID, 'about_unit_h', true);
				$data[$cont]['text_h'] 		= 	get_post_meta($values->ID, 'about_text_h', true);
				$data[$cont]['cant_p'] 		= 	get_post_meta($values->ID, 'about_cant_p', true);
				$data[$cont]['unit_p'] 		= 	get_post_meta($values->ID, 'about_unit_p', true);
				$data[$cont]['text_p'] 		= 	get_post_meta($values->ID, 'about_text_p', true);
				$cont++;
			}
		}
		return($data);
	}

	function add_about_metaboxes(){
		add_meta_box('about_metaboxes', 'Contador', 'about_metaboxes_fcn', 'about_page');
	}

	function about_page_func(){
		$tpl 		= '<div class="about_content"><ul>[list]</ul></div>';
		$item 		= '<li class="item item_0{cont}{content} animation"><h2>{title}</h2><div class="text-content">{text}</div>{svg}{counter}<div class="image_wrap" style="background-image:url(\'{imagen}\')"></div></li>';
		$contador 	= '<div class="number"><h6><span class="numscroller" data-max="{number}">0</span>{unit}</h6><p>{number_text}</p><a class="next" href="#" data_url=".item_0{next}">Next</a></div>';
		$data 		= get_about_items();
		$html 		= '';
		if(count($data)){
			foreach ($data as $key => $value) {
				$cont_changes = array(
					'{number}'		=> $value['cant_p'],
					'{unit}'		=> $value['unit_p'],
					'{number_text}'	=> $value['text_p'],
					'{next}'		=> intval($key) + 1,
				);

				$changes = array(
					'{cont}' 		=> $key,
					'{title}' 		=> $key < 4 ? '<span>'.$value['title'].'</span>' : '<span>'.$value['title'].'</span><a class="next" href="#" data_url=".item_01">First</a>',
					'{text}' 		=> $value['contenido'],
					'{svg}' 		=> $key < 4 ? get_circle_animation('about_timer_'.$key,false,'#fff') : '',
					'{content}'		=> $key < 4 ? ' content_about counters_block' : '',
					'{counter}'		=> $key < 4 ? strtr($contador,$cont_changes) : '',
					'{imagen}'		=> $value['img'],
				);
				$html.= strtr($item,$changes);
			}
			return str_replace("[list]",$html,$tpl);
		} else {
			return "";
		} 
	}

	// about_metaboxes
	function about_metaboxes_fcn() {
		global $post, $about_fields, $about_values;

		//cogemos los custom fields del post actual
		foreach($about_fields as $item){
			$about_values[$item]	 = get_post_meta($post->ID, $item, true);
		}
		
?>
		<input type="hidden" name="about_metaboxes_noncename" id="about_metaboxes_noncename" value="<?php echo wp_create_nonce('about_metaboxes'); ?>" />
		<table class="wp-list-table widefat fixed">
			<thead>
				<tr>
					<th>Home slider</th>
					<th>About page</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>
						<label>
							<strong>Cantidad:</strong>
							<input type="text" onkeyup="this.value=this.value.replace(/[^\d]/,'')" name="about_cant_h" value="<?php echo $about_values["about_cant_h"]; ?>" class="widefat" />
						</label>
					</td>
					<td>
						<label>
							<strong>Cantidad:</strong>
							<input type="text" onkeyup="this.value=this.value.replace(/[^\d]/,'')" name="about_cant_p" value="<?php echo $about_values["about_cant_p"]; ?>" class="widefat" />
						</label>
					</td>
				</tr>
				<tr>
					<td>
						<label>
							<strong>Unidad:</strong>
							<input type="text" name="about_unit_h" value="<?php echo $about_values["about_unit_h"]; ?>" class="widefat" />
						</label>
					</td>
					<td>
						<label>
							<strong>Unidad:</strong>
							<input type="text" name="about_unit_p" value="<?php echo $about_values["about_unit_p"]; ?>" class="widefat" />
						</label>
					</td>
				</tr>
				<tr>
					<td>
						<label>
							<strong>Texto:</strong>
							<input type="text" name="about_text_h" value="<?php echo $about_values["about_text_h"]; ?>" class="widefat" />
						</label>
					</td>
					<td>
						<label>
							<strong>Texto:</strong>
							<input type="text" name="about_text_p" value="<?php echo $about_values["about_text_p"]; ?>" class="widefat" />
						</label>
					</td>
				</tr>
			</tbody>
		</table>
<?php
	}

	function about_save_data(){
		global $meta_box, $about_fields, $post;		
		//verify nonce
		if (!wp_verify_nonce($_POST['about_metaboxes_noncename'],'about_metaboxes')) {
			return $post->ID;
		}
		
		foreach($about_fields as $item){
			update_post_meta($post->ID, $item, $_POST[$item]);
		}
	}
?>