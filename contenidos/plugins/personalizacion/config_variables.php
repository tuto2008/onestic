<?php
// create custom plugin settings menu
//add_action('admin_menu', 'my_cool_plugin_create_menu');
add_action("admin_menu", "add_onestic_config");

function add_onestic_config() {

	//create new top-level menu
	//add_menu_page('My Cool Plugin Settings', 'Cool Settings', 'administrator', __FILE__, 'onestic_settings_page' , plugins_url('/images/icon.png', __FILE__) );
	add_menu_page("Configuración", "Configuración", "manage_options", "onestic-config", "onestic_config_page", null, 40);

	//call register settings function
	add_action( 'admin_init', 'register_onestic_settings' );
}


function register_onestic_settings() {
	//register our settings
	register_setting('onestic-group', 'ContactFormEmail' );
	register_setting('onestic-group', 'ContactFormSubject' );
	register_setting('onestic-group', 'HomePageContactBackground' );
	register_setting('onestic-group', 'HomePageJobsBackground' );
}

function onestic_config_page() {
?>
<style>
	.config_wrap fieldset{float:left;display:inline;width:100%;border-top:1px solid #23282D;padding:16px 0;margin-top:20px}
	.config_wrap fieldset legend{padding:0 10px 0 0;text-transform:uppercase;font-size:1.3em}
	.config_wrap fieldset label{float:left;display:inline;width:100%;margin-top:10px}
	.config_wrap fieldset label strong{float:left;display:inline;width:15%;padding-top:4px;font-size:1.1em}
	.config_wrap fieldset label input{float:right;display:inline;width:calc(85% - 10px)}
	.config_wrap fieldset label p.extra{float:left;display:inline;width:75%;padding:0 0 0 16%;margin: 0;color:#A0A0A0}
</style>
<div class="wrap config_wrap">
<h2>Configuración de la página de Onestic</h2>

<form method="post" action="options.php">
    <?php settings_fields( 'onestic-group' ); ?>
    <?php do_settings_sections( 'onestic-group' ); ?>
    <fieldset>
    	<legend>Formulario de Contacto</legend>
	    <label>
	    	<strong>Destinatario:</strong>
	    	<input type="text" name="ContactFormEmail" value="<?php echo esc_attr( get_option('ContactFormEmail') ); ?>" />
	    	<p class="extra">Dirección email que recibirá los formularios de contacto</p>
	    </label>
	    <label>
	    	<strong>Asunto:</strong>
	    	<input type="text" name="ContactFormSubject" value="<?php echo esc_attr( get_option('ContactFormSubject') ); ?>" />
	    	<p class="extra">Asunto del email de contacto</p>
	    </label>
    </fieldset>
    <fieldset>
    	<legend>Página de inicio</legend>
	    <label>
	    	<strong>Imagen Contacto:</strong>
	    	<input type="text" name="HomePageContactBackground" value="<?php echo esc_attr( get_option('HomePageContactBackground') ); ?>" />
	    	<p class="extra">Imagen de fondo para el módulo de contacto de la home. Sube una imagen de 800x490 px y/o pega en este campo la url</p>
	    </label>
	    <label>
	    	<strong>Imagen Empleo:</strong>
	    	<input type="text" name="HomePageJobsBackground" value="<?php echo esc_attr( get_option('HomePageJobsBackground') ); ?>" />
	    	<p class="extra">Imagen de fondo para el módulo de Empleo de la home. Sube una imagen de 1400x800 px y/o pega en este campo la url</p>
	    </label>
    </fieldset>
    <?php submit_button(); ?>

</form>
</div>
<?php } ?>