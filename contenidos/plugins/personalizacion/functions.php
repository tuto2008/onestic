<?php 
	function body_id(){
		global $post;
		if(is_home() || is_404() || is_author() || is_search()){
			echo "id=\"home\" ";
		} else if(is_search()){
			echo 'id="blog" class="press"';
		} else if(is_archive()){
			echo 'id="projects" class="projects"';
		} else if(is_tag()){
			echo "id=\"etiqueta\" ";
		} else if(is_single()){
			$post_data = get_post($post->ID, ARRAY_A);
			$slug = str_replace("-","_",$post_data['post_name']);
			switch($post_data["post_type"]){
				case 'workshop':
					$add 		= 'id="'.$slug.'" class="post workshops"';
				break;
				
				case 'wpsc-product':
					$add 		= 'id="tienda" class="tienda"';
				break;
				
				default:
					if($post_data["post_category"] == 76){
						$add 		= 'id="blog" class="post press"';
					} else {
						$add 		= 'id="tutorials" class="post press"';
					}
				break;
			}	
			echo $add;
		} else if(is_page()){
			$post_data = get_post($post->ID, ARRAY_A);
			//var_dump($post_data);
			if( isset($post_data) ){
				$slug = str_replace("-","_",$post_data['post_name']);
				$slug = $slug == 'inicio' || $slug == "" ? 'home': $slug;
				if($post->post_parent != 0){
					$slug.= '" class="'.the_slug($post->post_parent);
				} else {
					$more = ($slug == 'blog' || $slug == 'tutorials') ? ' post post_list' : '';
					$slug.= '" class="'.$slug.$more;
				}
				echo 'id="'.$slug.'"';
			} else {
				// es una sección de la tienda que no tiene artículos
				echo 'id="tienda" class="tienda"';	
			}
		}
	}

	function the_slug($id) {
		$post_data = get_post($id, ARRAY_A);
		$slug = $post_data['post_name'];
		return str_replace("-","_",$slug); 
	}

	function current_lang(){
		/* saber el idioma actual */
	    $languages = icl_get_languages('skip_missing=0');
	    $current = "";
	    foreach($languages as $key=>$values) {
	        if($values["active"] == 1){$current = $key;}
	    }
	    return ($current);
	}

	/* team functions */
	function get_external_links_team($data){
		if(isset($data[0])){
			$tpl 	= '<div class="external_links"><ul>[list]</ul></div>';
			$item 	= '<li class="item item_0[cont] animation"><h3>[title]</h3><p>[text]</p><p class="link"><a href="[link]">[link_text]</a></p></li>';
			$html 	= "";
			$links 	= explode("|",$data[0]);
			$cont 	= 1;
			foreach($links as $key=>$vals){
				list($title,$intro,$link,$url) = explode("::",$vals);
				$vars = array(
					"[cont]" 		=> $cont,
					"[link]" 		=> $url,
					"[title]" 		=> $title,
					"[text]" 		=> $intro,
					"[link_text]" 	=> $link,
				);
				$html.= strtr($item,$vars);
				$cont++;
			}
			$html = str_replace("[list]",$html,$tpl);
			echo $html;
		} 
	}

	function link_to($ID,$show = true){
		if($show){
			echo get_page_link($ID);
		} else {
			return get_page_link($ID);
		}
	}

	function wrap_text($text,$item){
		return '<'.$item.'>'.$text.'</'.$item.'>';
	}
	/* fin team functions */

	function get_circle_animation($id,$mini = true,$border_c = '#000000'){
		if($mini){
			$grosor 	= 14;
			$separa 	= '1,13.5';
			$count_w	= '2';
			$count_r	= '61';
		} else {
			$grosor 	= 2.4;
			$separa 	= '0.3,2.6025';
			$count_w	= '0.7';
			$count_r	= '71';
		}
		return '<svg id="'.$id.'" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 172 172" preserveAspectRatio="none"><circle stroke-dasharray="0,5000" cx="86" cy="86" r="'.$count_r.'" class="count" fill="none" stroke="#fff" stroke-width="'.$count_w.'" transform="rotate(-90,86,86)" /><circle fill="none" stroke="'.$border_c.'" stroke-width="'.$grosor.'" stroke-miterlimit="100" stroke-dasharray="'.$separa.'" cx="86" cy="86" r="74" transform="rotate(-90,86,86)"/></svg>';
	}
?>