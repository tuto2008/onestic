<?php 
	add_action('init', 						'register_team', 0);
	add_shortcode('team_list', 				'team_list_func' );
	add_action('add_meta_boxes', 			'add_team_metaboxes' );
	add_action('save_post',					'team_save_data');

	$team_fields 							= array('cargo');
	$team_values 							= array();

	function register_team() {
		$labels = array(
				'name'               => 'Personal',
				'singular_name'      => 'Personal',
				'menu_name'          => 'Personal',
				'name_admin_bar'     => 'Personal',
				'add_new'            => 'Nuevo',
				'add_new_item'       => 'Agregar Personal',
				'new_item'           => 'Nuevo Personal',
				'edit_item'          => 'Editar Personal',
				'view_item'          => 'Ver Personal',
				'all_items'          => 'Todo el equipo',
				'search_items'       => 'Buscar Personal',
				'parent_item_colon'  => 'Personal padre:',
				'not_found'          => 'Personal no encontrado.',
				'not_found_in_trash' => 'Personal no encontrado en la papelera.'
			);

		$args = array(
			'labels'             	=> $labels,
	        'description'        	=> 'Descripción',
			'public' 				=> true,
			'exclude_from_search' 	=> true,
			'publicly_queryable'  	=> false,
			'show_ui'            	=> true,
			'show_in_nav_menus'   	=> true,
			'show_in_menu'       	=> true,
			'query_var'           	=> true,
			'rewrite'            	=> array( 'slug' => 'team' ),
			'capability_type'    	=> 'post',
			'has_archive'        	=> false,
			'hierarchical'       	=> false,
			'menu_position'      	=> 4,
			'menu_icon'   			=> 'dashicons-id',
			'supports'           	=> array('title', 'thumbnail', 'page-attributes')
		);
	    register_post_type( 'team', $args );
	}

	function add_team_metaboxes(){
		add_meta_box('team_metaboxes', 'Cargo', 'team_metaboxes_fcn', 'team');
	}

	// team_metaboxes Metabox
	function team_metaboxes_fcn() {
		global $post, $team_fields, $team_values;

		//cogemos los custom fields del post actual
		foreach($team_fields as $item){
			$team_values[$item]	 = get_post_meta($post->ID, $item, true);
		}
		
		//campo oculto de verificación
		echo '<input type="hidden" name="team_metaboxes_noncename" id="team_metaboxes_noncename" value="' .wp_create_nonce('team_metaboxes'). '" />';
		echo '<input type="text" name="cargo" value="'.$team_values["cargo"].'" class="widefat" />';
	}

	function team_save_data(){
		global $meta_box, $team_fields, $post;		
		//verify nonce
		if (!wp_verify_nonce($_POST['team_metaboxes_noncename'],'team_metaboxes')) {
			return $post->ID;
		}
		
		foreach($team_fields as $item){
			update_post_meta($post->ID, $item, $_POST[$item]);
		}
	}

	function team_list_func( $atts ) {
		global $post;
		$args	= array(
		  'post_type' 			=> 'team',
		  'post_status' 		=> 'publish',
		  'posts_per_page' 		=> -1,
		  'orderby'				=> 'menu_order',
		  'order'				=> 'ASC'
	    );
	    $my_query = new WP_Query($args);

		if( $my_query->have_posts() ) {

			$tpl 			= '<section class="module"><ul class="team">[list]</ul></section>';
			$item 			= '<li class="item animation" id="user{cont}"><img src="{image}" alt="{nombre} - {cargo}"><div class="data"><h2>{nombre}</h2><p>{cargo}</p></div></li>';
			$html 			= "";
			$cont 			= 1;

			foreach ($my_query->posts as $key => $values) {
				$pic 		= wp_get_attachment_image_src( get_post_thumbnail_id( $values->ID ) , 'team');
				$data 		= array(
					'{cont}'	=> $cont,
					'{nombre}'	=> $values->post_title,
					'{cargo}'	=> get_post_meta($values->ID, 'cargo', true),
					'{image}'	=> $pic[0]
				);
				$html.= strtr($item,$data);
				$cont++;
			}
			wp_reset_query();
			return str_replace("[list]",$html,$tpl);
		} else {
			// pendiente de ajustar a diseño del cliente
			return "No hay personas publicadas";
		}
	}
?>