$(function(){
    var params = {
        limit: 1,
        showThumbs: true,
        uploadFile: {
            url: base_url + "contenidos/applicants/php/upload.php",
            data: null,
            type: 'POST',
            enctype: 'multipart/form-data',
            beforeSend: function(){},
            success: function(data, el){
                var parent          = el.find(".jFiler-jProgressBar").parent();
                var current_file    = el.closest('label.file').find('.jFiler-item-title').attr('title');
                el.closest('label.file').find('input.upload_data').val(current_file);
            },
            error: function(el){
                var parent = el.find(".jFiler-jProgressBar").parent();
                el.find(".jFiler-jProgressBar").fadeOut("slow", function(){
                    $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");    
                });
            },
            statusCode: null,
            onProgress: null,
            onComplete: null
        },
        onRemove: function(itemEl, file, id, listEl, boxEl, newInputEl, inputEl){
            var file = file.name;
            $.post(base_url + 'contenidos/applicants/php/remove_file.php', {file: file});
        },
        captions: form_translate
    }

    params.captions.feedback = feedback_CV_file_data;
    $('input#CV_file').filer(params);
    
    params.captions.feedback = feedback_other_file_data;
    $('input#other_file_CV').filer(params);
});