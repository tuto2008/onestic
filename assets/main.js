$ajax_url = base_url + 'wp-admin/admin-ajax.php';
var files;
$crono_time = false;
$crono_time1 = $crono_time2 = $crono_time3 = false;
$(window).load(function() {
	ajustar();
	on_scroll_change();  //function in motion.js
});
$(window).scroll(function(){
	on_scroll_change();  //function in motion.js
})
$(window).resize(ajustar);
$(window).bind('orientationchange',ajustar);

$(function(){
	$('a').click( change_page_FCN );
	$('#popup a.close').click(close_popup);
	$('a.open_popup, .open_popup a').click(open_popup);
	$('#menu_button').click(open_popup);
	$('ul.nav_list li.active a').click(no_menu_link);
	$('.services_content li').click(go_services);
	$('.system_accordion li.item').click(show_accordion_info);
	$('.about_content a.next').click(next_about);
	$('a.more_info').click(open_moreinfo);
	$('#lang_sel_list a').click(changeLanguagePage);
	$('.content_careers li').on('click', 'a.close', close_moreinfo);
	$('em.email').each(mail_format);
	$('label.legal input').change(check_legal);
	$('a.mobile_expand_menu').click(open_menu_mobile);
	$('form.validate_form').submit(function(e){
		e.preventDefault();
		validate_form($(this));
	});
	$('.number').keypress(function (event) {
	    var keycode = event.which;
	    if (!(event.shiftKey == false && (keycode == 46 || keycode == 8 || keycode == 37 || keycode == 39 || (keycode >= 48 && keycode <= 57)))) {
	        event.preventDefault();
	    }
	});
	$('label.file div.upload input').change(update_fieldfile_content);
	ajustar();
});

function change_page_FCN(){
	if( ($(this).hasClass('ChangePage') || $(this).parent().hasClass('ChangePage')) && !$(this).parent().hasClass('open_popup') ){
		hide_current_page();
	}
}

function changeLanguagePage(){
	hide_current_page();
}

function hide_current_page(){
	$(document.body).fadeOut(300);
}

function no_menu_link(){
	return false;
}

function mail_format(){
	var dire = $(this).html();
	dire = dire.split("|");
	dire = dire[2]+"@"+dire[1]+"."+dire[0];
	$(this).replaceWith('<a class="mailto_link" href="mailto:'+dire+'">'+dire+'</a>');	
}

function show_accordion_info(){
	$current = $(this);
	if( !$current.hasClass("active") ){
		$('ul.items_group li.item').removeClass('active');
		$('ul.list').css('display','none');
		$current.addClass('active');
		$current.find('ul.list').slideDown(450);
	} else {
		$obj = $current.find('ul.list');
		$obj.slideUp(450,function(){
			$obj.css('display','none');
			$current.removeClass('active');
		});
	}
	return false;
}

function next_about(){
	obj = $($(this).attr('data_url'));
	var top = obj.offset().top - $('header').height();
	$("html, body").animate({ scrollTop: top }, 1000);
	return false;
}

function update_fieldfile_content(event){
	$(this).closest('label.file').find('input.field').val($(this).val());
	files = event.target.files;
}

function check_legal(){
	change_checked($(this));
}

function change_checked(current){
	var obj = current.closest('label').find('strong span');
	if( current.is(':checked') ){
		obj.addClass('checked');
		hide_error_msg();
	} else {
		obj.removeClass('checked');
	}	
}

function open_menu_mobile(){
	if( !$('ul.sections').is(':animated') ){
		if( !$('ul.sections').is(":visible") ){
			$('ul.sections').slideDown(300);
		} else {
			$('ul.sections').slideUp(300);
		}
	}
	return false;
}

function validate_form(form){
	var id = form.attr('id');
	$current_form = $('#'+id);
	if (check_fields_form($current_form) ){
		switch(id){
			case 'form_contact':
				//enviamos el formulario de contacto por email
				var data = $current_form.serialize();
				jQuery.post($ajax_url, data, function(response) {
					var data = JSON.parse(response);
					ProcessDataForm(data);
				});
			break;

			case 'form_applyCV':
				var data = $current_form.serialize();
				jQuery.post($ajax_url, data, function(response) {
					var data = JSON.parse(response);
					ProcessDataForm(data);
				});
			break;
		}
	};
} 

function ProcessDataForm(data){
	var form = $('#'+data.form);
	form.find('input.btn_send').slideUp(400);

	if(data.status == 'OK'){
		
		//vaciamos el campo legal
		form.find('.legal input').prop( "checked", false ).each(function(){
			change_checked($(this));
			form.find('.legal').addClass('hideLabel');
		});

		//vaciamos los campos de formulario
		form.find('.field').each(function(){
			$(this).val('');
			$(this).closest("label").addClass('hideLabel');
		});

		//vaciamos los campos file (CV)
		form.find('label.file').each(function(){
			$(this).find('.upload_data').val('');
			$(this).find('.jFiler-row').remove();
			var placeholder = $(this).find('.upload_field').attr('placeholder');
			$(this).find('.jFiler-input-caption span').html(placeholder);
			$(this).addClass('hideLabel');
		});
	} else {
		form.find('.EndMsgText').addClass('error');
	}

	//mostramos mensaje final y cerramos el popup
	form.find('.EndMsgText').html(data.msg).slideDown(500).delay(4500).slideUp(500,function(){
		if(data.status == 'OK'){
			close_popup();
		}
		form.find('input.btn_send').slideDown(400);
		form.find('.EndMsgText').removeClass('error');
	});

}

function check_fields_form(form){
	var form_error = false;
	hide_error_msg();
	form.find('.required').each(function(){
		var target = $(this).find('input, textarea');
		if( target.val().length <2 ){
			text = target.attr('data_error');
			show_error_msg($(this),text);
			target.focus();
			form_error = true;
			return false;
		}
	});
	if( !form_error ){
		var legal_target = form.find('label.legal');
		text = legal_target.find('input').attr('data_error');
		if( !legal_target.find('input').is(':checked') ){
			show_error_msg(legal_target,text);
			return false;
		} else {
			return true;
		} 
	}
}

function retr_dec(args) {
  return /\./.test(args) && args.match(/\..*/)[0].length - 1 || 0 ;
}

function animate_circle(circle_id,timer_id){
	var circle 					= $('#'+circle_id).find(".count"); //$(".count");
	var interval 				= 50;
	var angle 					= 0;
	var angle_increment 		= 6;
	var complete_fac			= 1.25
	var pass  					= 1;
	var max_pass				= 4;
	var obj 					= {};
	obj.id 						= timer_id;

	if(circle_id == 'circle_timer'){
		//Projects
		circle.attr("stroke","#282727");
		complete_fac		= 1.12;
	} else {
		if( circle.closest("li").hasClass('content_about') ){
			//About
			circle.attr("stroke","#fff");
			complete_fac		= 1.30;
			max_pass			= 2;
		} else {
			interval 			= 25;
			angle_increment 	= 1;
			max_pass			= 18;
			circle.attr("stroke","#CEDF00");
		}
	}
	var fac_inc					= 360 / angle_increment;

	$('.numscroller').each(function(){
		current_interval = $(this).attr('data-max') / fac_inc;
		current_interval = current_interval < 1 ? current_interval.toFixed(2) : Math.floor(current_interval);
		$(this).attr('data_interval',current_interval);
		$(this).attr('data_current',0);
		$(this).attr('data_decimal', retr_dec($(this).attr('data-max')) );
	});
	circle.attr("stroke-dasharray", "0, 5000");

	obj.timer = window.setInterval(function () {
		if(pass == max_pass){
			circle.attr("stroke-dasharray", angle * complete_fac + ", 5000");
			pass = 1;
		}
		
		//$('.numscroller').each(function(){
		circle.closest(".counters_block").find('.numscroller').each(function(){
			current_number = parseFloat($(this).attr('data_current')) + parseFloat($(this).attr('data_interval'));
			$(this).attr('data_current',current_number);
			current_number = $(this).attr('data_decimal') == 0 ? parseInt(current_number) : current_number.toFixed($(this).attr('data_decimal'));
			$(this).html( current_number );
		});
		if (angle >= 360) {
			window.clearInterval(obj.timer);
			circle.closest(".counters_block").find('.numscroller').each(function(){
				$(this).html( $(this).attr('data-max') );
			});

			//continuamos con el slider si estamos en una animación de la home
			if( circle.closest("li").hasClass('ani') ){
				$('.home_slider_top ul.slider').cycle('resume'); 
			}
		}
		pass++;
		angle += angle_increment;
	}.bind(this), interval);
}

function validateEmail($email) {
	var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	return emailReg.test( $email );
}

function show_error_msg(obj,text){
	obj.prepend('<p class="msg_error">'+text+'</p>').each(function(){
		$('p.msg_error').slideDown(400);
	});	
}

function hide_error_msg(){
	$('p.msg_error').slideUp(400,function(){
		$(this).remove();
	});
}

function go_services(){
	hide_current_page();
	window.location = $(this).find("h2 a").attr('href');
	return false;
}

function open_popup(){
	var obj = $(this).attr('href');
	$show_popup = ($("#popup").css('visibility') == 'hidden') ? true : false;
	if($show_popup){
		$('.hideLabel').removeClass('hideLabel');
		$('#popup').css('display','inline');
		// el popup esta cerrado y procedemos a abrirlo
		$('.modal').css('height','100%');
		adjust_popup_size(obj,'slow');
		$('#popup').css('display','none').css('visibility','visible');
		$('.popup_section').css('display','none');
		//$(obj).show();
		$("#popup").fadeIn(300,function(){
			$(obj).fadeIn(700);
		}).addClass('active');

		//Si el usuario quiere aplicar sobre una oferta de empleo, cogemos el id de la oferta
		//y lo agregamos al campo oculto del formulario Send CV	
		if($(this).parent().hasClass('apply')){
			$('input#data_jobid').val( $(this).attr('data_jobid') );
		}
	} else {
		// el popup ya esta abierto y hay que cambiar de contenido
		$('.popup_section').fadeOut(300,function(){
			$(obj).slideDown(700);
			if(adjust_popup_size(obj,0)){
				
			}
		});
	}
	return false;
}

function adjust_popup_size(obj,anitop_time){
	if($(obj).outerHeight() > $h){
		$("html, body").animate({ scrollTop: 0 }, anitop_time,function(){
			$('#popup').addClass('absolute');
			$less = $("body.empleo, body.careers").length ? 100 : 0;
			$('.modal').css('height',$(obj).height() - $less);
		});
	} else {
		$('#popup').removeClass('absolute');
		$('.modal').css('height',$h);	
	}
	return true;
}

function close_popup(){
	$('.modal').css('height','100%');
	$("#popup").removeClass('active').fadeOut(300,function(){
		$('#popup').css('display','none').css('visibility','hidden');
		$('.popup_section').css('display','table');
	});
	return false;
}

function apply_CV(){
	return false;
}

function open_moreinfo(){
	$(this).closest('li.item').addClass('active');
	$(this).parent().find('.list').slideDown(700);
	return false;	
}

function close_moreinfo(){
	$(this).closest('li.item').removeClass('active');
	$(this).parent().find('.list').slideUp(700);
	return false;		
}

function ajustar(){
	$('body').removeClass('horizontal').removeClass('vertical');
	$w 			= $(window).width();
	$h 			= $(window).height();
	$body 		= $w>($h*1.10) ? 'horizontal' : 'vertical';
	$('body').addClass($body);
	$('div#main_menu').height($h);
	ajustar_services();
	ajustar_about();
	ajustar_team();
	ajustar_projects();
	ajustar_projects_detail();
	ajustar_home();
	ajustar_project();
	ajustar_careers();
}

function ajustar_team(){
	if($('ul.team').length){
		$("ul.team li").each(function() {
			$obj 	= $(this).find('.data');
			$obj.css('padding-top',0).css('padding-bottom',0);
			full 	= $(this).find('img').outerHeight();
			name 	= $obj.outerHeight();
			separa 	= ((full - name) / 2);
			$obj.css('padding-top',(separa*1.10)).css('padding-bottom',(separa*0.90));
		});
	}
}

function ajustar_project(){
	if($('body.post').length){

		//ajustamos el tamaño de los videos al 100% de ancho por la proporción en alto para el responsive
		$('section.post_content iframe').each(function(){
			var alto = '';
			if( !$(this).attr('data_factor')){
				var factor = $(this).height() / $(this).width();
			} else {
				factor = $(this).attr('data_factor');
			}
			alto = $('section.post_content').width() * factor;
			$(this).css('width','100%').css('height',alto).attr('data_factor',factor);
		});
	}
}

function ajustar_services(){
	if($('body#servicios, body#services').length){
		$(".services_content").addClass('animation');
		var x = 1;
		$(".services_content li").each(function() {
			$(this).css('padding-top',0).css('padding-bottom',0);
			ancho 	= $(this).outerWidth();
			alto 	= $(this).outerHeight();
			separa 	= ((ancho - alto) / 2);
			$(this).css('padding-top',(separa*0.90)).css('padding-bottom',(separa*1.10));
			$(this).addClass('animation');
			if(x==2){
				$(this).addClass('restar');
			}
			++x;
		});
	}
	if($('.system_accordion').length){
		$(".system_accordion li.item").each(function() {
			$(this).addClass('animation').addClass('noDelay');
		});
	}
}

function ajustar_about(){
	if($('body.about, body.acerca').length){
		$(".about_content li").each(function() {
			$obj = $(this).find('h2');
			$obj.css('margin-top',0);
			$tot = $(this).outerHeight();
			$tp = $(this).find('.text-content').outerHeight();
			$th2 = $obj.outerHeight();
			$top = $tot - ($tp + $th2);
			$obj.css('margin-top',$top);

			//centramos el círculo animado
			$(this).find('svg').height($tot);
			$left = ( $(this).width() - $(this).find('svg').width() )/2;
			$(this).find('svg').css('margin-left',$left);

			//escalamos la imagen de fondo
			$(this).find('.image_wrap').width( $(this).width() ).height( $(this).height() );
		});

		$('.offices_list li.address').each(function () {
			var dire = $(this).find('address span').html();
			var link = "http://maps.google.com/maps?q=" + encodeURIComponent( dire );
			$(this).find('p.google_link a').attr('href',link);
			$(this).addClass('animation');
		});
	}	
}

function ajustar_projects(){
	if($('body.projects, body.proyectos').length){
		$('#project_list').removeClass('animate');
		$('#project_list li').css('height','auto').css('width','33.33333%');
		$('#project_list li .data').css('width','auto').css('height','auto');

		/* usaremos esta variable para saber si el listado en archivo o principal */
		$isMain = $('#project_list').hasClass('main') ? true : false ;
		var mod = Math.floor($('#project_list ul').outerWidth() / 3);
		var cont = 1;
		var total_items = $('#project_list li.item').length;
		var items = total_items % 9 == 0 ? 9 : total_items % 9;
		var last_row = Math.floor(total_items/9);
		$('#project_list').addClass('items_' + items);
		$('#project_list li').each(function(){
			if((cont%9 == 1 || cont%9 == 6) && $w>481 && $isMain){
				$(this).css('width','66.66666%');
			} else {
				$(this).css('width','33.33333%');
			}
			$(this).css('height','auto');
			if($w>481){
				if(cont%9 == 3 && !$(this).hasClass('archive_link') && $isMain){
					if( Math.floor(cont/9) != last_row || (Math.floor(cont/9) == last_row && total_items%9 >5) ){ 
						$(this).height((mod*2)-0.04);
					} else {
						$(this).height(mod);
					}
				} else {
					$(this).height(mod);
				}
			} else {
				$(this).height((mod*3)/2);
			}
			$(this).find('h2').css('padding-top',0);
			var h2top 	= $(this).hasClass('archive_link') ? 1 : 1.2;
			var alto 	= $(this).find('h2').outerHeight();
			var mar 	= ($(this).height() - alto) / 2;
			$(this).find('h2').css('padding-top',mar*h2top);
			$(this).find('.data').width($(this).width()).height($(this).height());
			cont++;
		});
		$('ul.sections li a').css('padding-left',0).css('padding-right',0);
		var tot = $('.project_list_content').width();
		var menu = $('ul.sections').width();
		var items = $('ul.sections li').length;
		var margin = Math.floor((tot - menu) / (items * 2));
		$('ul.sections li a').css('padding-left',margin).css('padding-right',margin);
		if($('ul.sections li.active').length == 0){
			$('ul.sections li').eq(0).addClass('active');
		}
		$('#project_list').addClass('animate');
	}
}

function ajustar_projects_detail(){
	if($('body.post').length){

		//quitamos párrafos vacios generados por el editor de texto de wordpress
		$(".post_content p").each(function(){
			if($(this).html().length == 0){
				$(this).remove();
			}
		});

		//agregamos animación a cada parrafo o video del contenido del proyecto
		$(".post_content iframe, .post_content p").addClass('animation');

		// Si el contenido del post no esta envuelto en un shortcode de tipo cols
		// agregamos uno al contenedor del contenido del post para ajustar a diseño del cliente
		if ( $(".post_content .cols_content").length == 0 ){
			$( ".post_content" ).wrapInner( '<div class="cols_content"></div>');
		}
	}
}

function ajustar_comment_list(){
	var max_h = 0;
	var LeftItem = 0;
	$('.comment_list ul').css('height','auto');
	$('.comment_list li').each(function(){
		max_h 		= max_h < $(this).outerHeight() ? $(this).outerHeight() : max_h;
		if( $(this).find('img.thumb').css('position') == 'absolute' ){
			$(this).css('display','inline').css('visibility','hidden');
			var top_img = $(this).find('img.thumb').outerHeight() / 2;
			$(this).find('img.thumb').css('margin-top',top_img*(-1));
			$(this).css('display','inline').css('visibility','visible');
		} else {
			$(this).find('img.thumb').css('margin-top',0).css('top',0);
		}
	});
	$('.comment_list ul').height(max_h);
}

function ajustar_home(){
	if($('body.home').length){
		$obj 			= $('.contact_link strong');
		$obj.css('padding-top',0).css('padding-bottom',0);
		var alto 		= $('.contact_link a').outerHeight();
		var strong_h 	= $obj.outerHeight();
		var marg_mobile	= $w>495 ? 1 : 0.76;
		var separa 		= ((alto - strong_h) / 2) * marg_mobile;
		$obj.css('padding-top',separa).css('padding-bottom',separa);

		if($('.home_text_list').length){
			/*
			$('.home_text_list li').removeClass('animation').removeClass('show');
			$('.home_text_list ul').css('height','auto');
			$('.home_text_list ul').css('height', $('.home_text_list ul').height() );
			*/
			$('.home_text_list li').addClass('animation show');
		}

		if($('.comment_list').length){
			circle_slide_function('destroy');
			circle_slide_function('active');
			ajustar_comment_list();
		}

		if($('ul.slider li').length){
			var min_h = 2.2;
			if( $('body').hasClass('vertical') ){
				var max_h = $h - $('header').outerHeight();
			} else {
				if($w/$h > min_h){
					var max_h = $w * 0.4;
				} else {
					var max_h = $h - $('header').outerHeight();
				}	
			}

			if( $w > 2025){
				var max_h = $('ul.slider li').width() * 0.55;
			} 

			$('ul.slider li, ul.slider li .img_content').height(max_h);

			if($('.slider_nav').length){
				$('.slider_nav').css('left',0);
				var marg = ($('ul.slider').outerWidth() - $('.slider_nav').outerWidth()) / 2;
				$('.slider_nav').css('left',marg);
			}

			var top 	= 0;
			$('ul.slider li').each(function(){
				this_obj 		= $(this).find('.info');
				$(this).addClass('resize');
				this_obj.css('padding-top',0);
				top 			= (max_h - this_obj.height()) / 2;
				factor_top 		= 0.9;

				/* ajustamos animación circular */
				if( $(this).hasClass('ani') ){
					this_svg 	= $(this).find('.img_content svg');
					svg_left 	= ($(this).width() - this_svg.width()) / 2;
					this_svg.css('margin-left',svg_left);
					if( $('body').hasClass('vertical') ){
						svg_top 	= (max_h - this_svg.height()) / 2;
						svg_top		= svg_top < 0 ? 0 : svg_top;
						this_svg.css('margin-top',svg_top);
						factor_top = 1;
					} else {
						this_svg.css('margin-top',0);
					}
				} 
				/* fin ajustes circular */

				this_obj.width( this_svg.width() );
				$margin_left = ($(this).width() - this_obj.width()) / 2
				this_obj.css('margin-left',$margin_left);
				$(this).removeClass('resize');
				$(this).find('.info').css('padding-top',top*factor_top);
			});
			$('ul.slider, ul.slider li').height(max_h);

			$('.home_slider_top ul.slider').cycle('destroy');
			$('.home_slider_top ul.slider').cycle({ 
				fx:     	'scrollHorz', //fade
				pager:  	'.slider_nav',
				speed:    	900, 
				timeout: 	6500,
				after:   	onAfter,
				before:  	onBefore
			});
		}
	}
}

function ajustar_careers(){
	if($('#apply_form').length){
		$('label.file').each(function(){
			var alto = $(this).find('input.field').outerHeight();
			$(this).find('.upload, .upload input').height(alto);
		});
	}
}

function circle_slide_function($status){
	switch ($status){
		case 'active':
			$('.comment_list ul').cycle({ 
				fx:     			'scrollHorz', 
				prev:   			'#prev_nav', 
				next:   			'#next_nav',
				speed:    			300, 
				timeout: 			0, 
				rev: 				true,
				after:   			onAfterComments,
				before:  			onBeforeComments
			});
		break;

		case 'destroy':
			$('.comment_list ul').cycle('destroy');
		break;
	}
}

//funciones al cambiar los comentarios de la home
function onBeforeComments(){
	var obj = $(this).find('img.thumb');
	obj.addClass('hide');
	ajustar_comment_list();
}

function onAfterComments() {
	var obj 		= $(this).find('img.thumb');
	var PosiLeft 	= obj.attr('data_left');
	obj.removeClass('hide');
}

//funciones al cambiar el slide de la home
function onBefore(){
	var obj = $(this);
	if(obj.hasClass('ani')){
		$('.home_slider_top ul.slider').cycle('pause');
		$crono_time = false;
		var current_ID = obj.find('svg').attr('id');
		for (var i = 1; i < 99999; i++){
			window.clearInterval(i);
		}
		obj.find('.numscroller').html(0);
		obj.find('svg circle.count').attr('stroke',"#fff");
	}
}

function onAfter() {
	var obj = $(this);
	if(obj.hasClass('ani')){
		var current_ID = obj.find('svg').attr('id');
		animate_circle(current_ID,'time_01');
	}
}
