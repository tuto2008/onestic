(function($){
	$.fn.visible = function(espera){
		_viewTop			= $(window).scrollTop();
		_viewBottom			= _viewTop + $(window).height();
		offset 				= $(this).offset();
		if( offset ){
			_top				= offset.top;
	    	_delay				= _top + ($(this).height() * 0.30); //esperamos un 40% antes de mostrar
	    	_compareTop			= espera === true ? _delay : _top;
			return (_viewBottom >= _compareTop);
		}
    };  
})(jQuery);