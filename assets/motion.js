function on_scroll_change(){
	//animamos los números de la ficha de proyectos la primera vez que hacemos scroll hasta ellos
	if($(".post_numbers li.center").length){
		if(!$crono_time && $(".post_numbers li.center").visible(true) ){
			animate_circle('circle_timer','time_02');
			$crono_time = true;
		}
	}

	//animamos los números de la página de about la primera vez que hacemos scroll hasta ellos
	if($('.about_content').length){
		if( $('.about_content li.item_01').visible(true) && !$crono_time1){
			animate_circle('about_timer_1','time_03');
			$crono_time1 = true;
		}
		if( $('.about_content li.item_02').visible(true) && !$crono_time2){
			animate_circle('about_timer_2','time_04');
			$crono_time2 = true;
		}
		if( $('.about_content li.item_03').visible(true) && !$crono_time3){
			animate_circle('about_timer_3','time_05');
			$crono_time3 = true;
		}
	}

	//animaciones standar en las páginas
	if($(".animation").length){
		var index = 0;
		var time_wait = 80;
		$('.animation').each(function() {
			wait_OnScroll = $(this).hasClass('.noDelay') ? false : true; 
			if( $(this).visible(wait_OnScroll) && !$(this).is(':animated') ){
				if($(this).css('position') == 'static'){
					$(this).addClass('addrelative');
				}
				var top 		= 0;
				var aniObj 		= '';
				var showAni 	= true;
				var speed		= 4;

				if( $(this).hasClass('restar') && $(window).width()<= 655){
					top = -51;
				}

				if( $(this).hasClass('show') ){
					var h = $(this).height();
					$(this).css('height','0').css('overflow','hidden').css('opacity',1);
					aniObj = {
						height: 		h,
						zIndex: 		index
					}
					showAni = (h == 0) ? false : true;
					speed		= 16;
				} else {
					$(this).css('top','120px');	
					aniObj = {
						opacity: 		1,
						top: 			top,
						zIndex: 		index
					}
				}
				if( $(this).hasClass('noTopAni') ){
					$(this).css('top',0);
				}

				if(showAni){
					$(this).delay(index*time_wait).animate(aniObj, 550 + (4*index), function(){
						$(this).removeClass('animation').removeClass('addrelative');
						if( $(this).hasClass('careers_link') ){
							var job_content = $('.careers_link .content');
							if( !job_content.is(':animated')){
								aniObj2 = {
									right: 		0,
									opacity: 	1
								}
								job_content.animate(aniObj2, 600);
							}
						}
					});
				}
			} 
			++index;
		});
	}

	var job_link = $('.careers_link a.link');
	if( job_link.visible(true) && !job_link.is(':animated')){
		aniObj = {
			left: 		0,
			opacity: 	1
		}
		job_link.animate(aniObj, 600);
	}
}

$(function(){
	$("body#services, body#servicios").mousemove(move_backgroud);
});

function move_backgroud(e){
	var movementStrength = 15;
	var width = movementStrength / $(window).width();
	var pageX = e.pageX - ($(window).width() / 2);
	var newvalueX = width * pageX * -1 - 15;
	$('#content').css("background-position", newvalueX+"px bottom");	
}